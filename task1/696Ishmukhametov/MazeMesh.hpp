#include <Mesh.hpp>

#include <iostream>
#include <vector>
#include <fstream>
#include <string>


void makeMaze(std::string filename, MeshPtr& maze, MeshPtr& ceiling, glm::vec2& size, glm::vec3& start) {
	std::vector<glm::vec3> maze_vertices;
	std::vector<glm::vec3> maze_normals;
	std::vector<unsigned int> maze_indices;

	std::vector<glm::vec3> ceil_vertices;
	std::vector<glm::vec3> ceil_normals;
	std::vector<unsigned int> ceil_indices;

	std::ifstream f;
	f.open(filename);

	char c1, c2;
	float x, y, z;

	f >> size.x >> size.y;
	f >> start.x >> start.y >> start.z;

	while (f >> c1 >> c2 >> x >> y >> z)
	{
		if (c1=='m' && c2=='v') 
			maze_vertices.push_back(glm::vec3(x*size.x, y*size.x, z*size.x));
		if (c1 == 'm' && c2 == 'n')
			maze_normals.push_back(glm::vec3(x, y, z));
		if (c1 == 'm' && c2 == 'f') {
			maze_indices.push_back(unsigned int(x));
			maze_indices.push_back(unsigned int(y));
			maze_indices.push_back(unsigned int(z));
		}

		if (c1 == 'c' && c2 == 'v')
			ceil_vertices.push_back(glm::vec3(x*size.x, y*size.x, z*size.x));
		if (c1 == 'c' && c2 == 'n')
			ceil_normals.push_back(glm::vec3(x, y, z));
		if (c1 == 'c' && c2 == 'f') {
			ceil_indices.push_back(unsigned int(x));
			ceil_indices.push_back(unsigned int(y));
			ceil_indices.push_back(unsigned int(z));
		}
	}

	f.close();

	//----------------------------------------

	DataBufferPtr maze_vert = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	maze_vert->setData(maze_vertices.size() * sizeof(float) * 3, maze_vertices.data());

	DataBufferPtr maze_norm = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	maze_norm->setData(maze_normals.size() * sizeof(float) * 3, maze_normals.data());

	DataBufferPtr maze_idx = std::make_shared<DataBuffer>(GL_ELEMENT_ARRAY_BUFFER);
	maze_idx->setData(maze_indices.size() * sizeof(unsigned int), maze_indices.data());

	maze = std::make_shared<Mesh>();
	maze->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, maze_vert);
	maze->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, maze_norm);
	maze->setIndices(maze_indices.size(), maze_idx);
	maze->setPrimitiveType(GL_TRIANGLES);
	maze->setVertexCount(maze_vertices.size());


	DataBufferPtr ceil_vert = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	ceil_vert->setData(ceil_vertices.size() * sizeof(float) * 3, ceil_vertices.data());

	DataBufferPtr ceil_norm = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	ceil_norm->setData(ceil_normals.size() * sizeof(float) * 3, ceil_normals.data());

	DataBufferPtr ceil_idx = std::make_shared<DataBuffer>(GL_ELEMENT_ARRAY_BUFFER);
	ceil_idx->setData(ceil_indices.size() * sizeof(unsigned int), ceil_indices.data());

	ceiling = std::make_shared<Mesh>();
	ceiling->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, ceil_vert);
	ceiling->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, ceil_norm);
	ceiling->setIndices(ceil_indices.size(), ceil_idx);
	ceiling->setPrimitiveType(GL_TRIANGLES);
	ceiling->setVertexCount(ceil_vertices.size());

	std::cout << "Maze is created with " << maze_vertices.size() << " vertices\n";
	std::cout << "Ceiling is created with " << ceil_vertices.size() << " vertices\n";
}