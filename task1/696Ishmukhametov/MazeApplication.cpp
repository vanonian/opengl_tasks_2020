#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>

#include "MazeMesh.hpp"
#include "MazeFreeCameraMover.hpp"

#include <iostream>
#include <vector>


class MazeApplication : public Application
{
public:
	MazeApplication() {
		_cameraMover = std::make_shared<OrbitCameraMover>();
	}

	CameraMoverPtr _secondaryCameraMover;

	MeshPtr _maze;
	MeshPtr _ceiling;

	ShaderProgramPtr _shader;

    GLuint _ubo;
    GLuint uniformBlockBinding = 0;

	bool _isCeilVisible = false;

	void handleKey(int key, int scancode, int action, int mods) override
	{
		if (action == GLFW_PRESS) {
			if (key == GLFW_KEY_C) {
				_isCeilVisible = !_isCeilVisible;
			} else if (key == GLFW_KEY_Q) {
				_cameraMover.swap(_secondaryCameraMover);
			} else {
				Application::handleKey(key, scancode, action, mods);
			}
		}
	}

    void makeScene() override
    {
        Application::makeScene();

		glm::vec2 maze_size;
		glm::vec3 maze_start;
		makeMaze("696IshmukhametovData1/maze.txt", _maze, _ceiling, maze_size, maze_start);

		_maze->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));
		_ceiling->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));
        
		_secondaryCameraMover= std::make_shared<MazeFreeCameraMover>(maze_start, maze_size);

		//=========================================================
        //Инициализация шейдеров

        _shader = std::make_shared<ShaderProgram>("696IshmukhametovData1/shader.vert", "696IshmukhametovData1/shader.frag");
    }

    void draw() override
    {
        Application::draw();

        //Получаем текущие размеры экрана и выставлям вьюпорт
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Подключаем шейдер
        _shader->use();

		_shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
		_shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        //Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку


		_shader->setMat4Uniform("modelMatrix", _maze->modelMatrix());
		_maze->draw();

		if (_isCeilVisible) {
			_shader->setMat4Uniform("modelMatrix", _ceiling->modelMatrix());
			_ceiling->draw();
		}
    }
};

int main()
{
    MazeApplication app;
    app.start();

    return 0;
}