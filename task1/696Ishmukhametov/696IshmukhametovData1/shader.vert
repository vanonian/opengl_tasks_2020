#version 330

uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform mat4 modelMatrix;

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 vertexNorm;

out vec4 color;

void main()
{
    color = vec4(vertexNorm*0.5 + 0.5, 1.0);

    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(vertexPosition, 1.0);
}
