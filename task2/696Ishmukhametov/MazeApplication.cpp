#include <Application.hpp>
#include <LightInfo.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include <Texture.hpp>

#include "MazeLayout.hpp"
#include "MazeMesh.hpp"
#include "MazeFreeCameraMover.hpp"
#include "MaterialInfo.hpp"

#include <iostream>
#include <sstream>
#include <vector>
#include <memory>


/**
Пример с текстурированием разных 3д-моделей
*/
class MazeApplication : public Application
{
public:
	MazeApplication() {
		_cameraMover = std::make_shared<OrbitCameraMover>();
	}

	MeshPtr _marker; //Маркер для источника света

	MazeMeshes _maze;
	bool _isCeilVisible = false;

	GLuint _ubo;
	GLuint uniformBlockBinding = 0;

	CameraMoverPtr _orbitCameraMover;
	CameraMoverPtr _freeCameraMover;

	//Идентификатор шейдерной программы
	ShaderProgramPtr _pointlight_shader;
	ShaderProgramPtr _spotlight_shader;
	ShaderProgramPtr _shader = _pointlight_shader;
	ShaderProgramPtr _markerShader;

	TexturePtr _wallTexture;
	TexturePtr _wallNormalMap;
	TexturePtr _floorTexture;
	TexturePtr _floorNormalMap;
	std::vector<TexturePtr> _posterTextures;
	GLuint _sampler;

	//Переменные для управления положением одного источника света
	float _lr = 20.0;
	float _phi = 0.0;
	float _theta = glm::pi<float>() * 0.25f;

	LightInfo _orb_light;
	LightInfo _head_light;
	LightInfo& _light = _orb_light;
	bool _isHeadLight = false;

	MaterialInfo _wall_mat;
	MaterialInfo _floor_mat;
	MaterialInfo _poster_mat;

	void handleKey(int key, int scancode, int action, int mods) override
	{
		if (action == GLFW_PRESS) {
			if (key == GLFW_KEY_C) {
				_isCeilVisible = !_isCeilVisible;
			}
			else if (key == GLFW_KEY_Q) {
				if (_isHeadLight) {
					_cameraMover = _orbitCameraMover;
				}
				else {
					_cameraMover = _freeCameraMover;
				}

				_isHeadLight = !_isHeadLight;
			}
			else {
				Application::handleKey(key, scancode, action, mods);
			}
		}
	}

    void makeScene() override
    {
        Application::makeScene();

        //=========================================================
        //Создание и загрузка мешей		

		_marker = makeSphere(0.1f);

		glm::vec3 maze_start;
		MazeLayout maze;
		_maze.makeMaze("696IshmukhametovData2/maze.txt", maze_start, maze);

		_maze._maze->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));
		_maze._ceiling->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));
		_maze._floor->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));
		for (int i = 0; i < _maze._numPosters; i++) {
			_maze._posters[i]->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));
		}

		_orbitCameraMover = std::make_shared<OrbitCameraMover>();
		_freeCameraMover = std::make_shared<MazeFreeCameraMover>(maze_start, maze);

        //=========================================================
        //Инициализация шейдеров

		_pointlight_shader = std::make_shared<ShaderProgram>("696IshmukhametovData2/shaders/texture.vert", "696IshmukhametovData2/shaders/pointlight_texture.frag");
		_spotlight_shader = std::make_shared<ShaderProgram>("696IshmukhametovData2/shaders/texture.vert", "696IshmukhametovData2/shaders/spotlight_texture.frag");
		_markerShader = std::make_shared<ShaderProgram>("696IshmukhametovData2/shaders/marker.vert", "696IshmukhametovData2/shaders/marker.frag");

		//=========================================================
        //Инициализация значений переменных освщения
        _orb_light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        _orb_light.ambient = glm::vec3(0.3, 0.3, 0.3);
        _orb_light.diffuse = glm::vec3(0.7, 0.7, 0.7);
        _orb_light.specular = glm::vec3(1.0, 1.0, 1.0);
		_orb_light.attenuation0 = 1.0;
		_orb_light.attenuation1 = 0.01;
		_orb_light.attenuation2 = 0.00004;

		_head_light.position = std::static_pointer_cast<MazeFreeCameraMover>(_freeCameraMover)->getPosition() + glm::vec3(0.0, 0.0, 0.25);
		_head_light.ambient = glm::vec3(0.3, 0.3, 0.3);
		_head_light.diffuse = glm::vec3(0.7, 0.7, 0.7);
		_head_light.specular = glm::vec3(1.0, 1.0, 1.0);
		_head_light.attenuation0 = 1.0;
		_head_light.attenuation1 = 0.02;
		_head_light.attenuation2 = 0.005;

		//=========================================================
		//Инициализация значений переменных материалов
		_wall_mat.Ka = glm::vec3(1.0, 1.0, 1.0);
		_wall_mat.Kd = glm::vec3(0.9, 0.9, 0.9);
		_wall_mat.Ks = glm::vec3(0.1, 0.1, 0.1);
		_wall_mat.shininess = 16.0;
		_wall_mat.haveNormalMap = true;

		_floor_mat.Ka = glm::vec3(1.0, 1.0, 1.0);
		_floor_mat.Kd = glm::vec3(0.8, 0.8, 0.8);
		_floor_mat.Ks = glm::vec3(0.5, 0.5, 0.5);
		_floor_mat.shininess = 128.0;
		_floor_mat.haveNormalMap = true;

		_poster_mat.Ka = glm::vec3(1.0, 1.0, 1.0);
		_poster_mat.Kd = glm::vec3(0.8, 0.8, 0.8);
		_poster_mat.Ks = glm::vec3(0.2, 0.2, 0.2);
		_poster_mat.shininess = 32.0;
		_poster_mat.haveNormalMap = false;
        
        //=========================================================
       
        //Загрузка и создание текстур
        _wallTexture = loadTexture("696IshmukhametovData2/textures/wall/texture.jpg");
		_wallNormalMap = loadTexture("696IshmukhametovData2/textures/wall/normal_map.jpg");
		_floorTexture = loadTexture("696IshmukhametovData2/textures/floor/texture.jpg");
		_floorNormalMap = loadTexture("696IshmukhametovData2/textures/floor/normal_map.jpg");
		_posterTextures = std::vector<TexturePtr>(_maze._numPosters);
		for (int i = 0; i < _maze._numPosters; i++) {
			_posterTextures[i] = loadTexture("696IshmukhametovData2/textures/posters/poster"+std::to_string(i)+".jpg");
		}
		
        //=========================================================
        //Инициализация сэмплера, объекта, который хранит параметры чтения из текстуры
        glGenSamplers(1, &_sampler);
        glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glSamplerParameterf(_sampler, GL_TEXTURE_MAX_ANISOTROPY_EXT, static_cast<GLfloat>(8));
    }

	void updateGUI() override
	{
		Application::updateGUI();

		ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
		if (ImGui::Begin("Light position", NULL, ImGuiWindowFlags_AlwaysAutoResize))
		{
			ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

			if (ImGui::CollapsingHeader("Light"))
			{
				ImGui::ColorEdit3("ambient", glm::value_ptr(_light.ambient));
				ImGui::ColorEdit3("diffuse", glm::value_ptr(_light.diffuse));
				ImGui::ColorEdit3("specular", glm::value_ptr(_light.specular));

				ImGui::SliderFloat("radius", &_lr, 0.1f, 40.0f);
				ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
				ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());
			}
		}
		ImGui::End();
	}

    void draw() override
    {
        //Получаем текущие размеры экрана и выставлям вьюпорт
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glEnable(GL_DEPTH_TEST);

		_orb_light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
		_head_light.position = std::static_pointer_cast<MazeFreeCameraMover>(_freeCameraMover)->getPosition() + glm::vec3(0.0, 0.0, 0.25);

		glm::vec3 lightPosCamSpace;
		if (_isHeadLight) {
			_light = _head_light;
			_shader = _spotlight_shader;
		}
		else {
			_light = _orb_light;
			_shader = _pointlight_shader;
		}

		lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 1.0));

		//Подключаем шейдер	
        _shader->use();

        //Загружаем на видеокарту значения юниформ-переменных
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

		_shader->setVec3Uniform("light.pos", lightPosCamSpace); //копируем положение уже в системе виртуальной камеры
		_shader->setVec3Uniform("light.La", _light.ambient);
		_shader->setVec3Uniform("light.Ld", _light.diffuse);
		_shader->setVec3Uniform("light.Ls", _light.specular);
		_shader->setVec3Uniform("light.a", glm::vec3(_light.attenuation0, _light.attenuation1, _light.attenuation2));


		GLuint textureUnitForDiffuseTex1 = 0;
		GLuint textureUnitForNormalMap1 = 1;
		GLuint textureUnitForDiffuseTex2 = 2;
		GLuint textureUnitForNormalMap2 = 3;
		GLuint textureUnitForPosters = 4;

        if (USE_DSA) {
			std::cout << "DSA\n";
            glBindTextureUnit(textureUnitForDiffuseTex1, _wallTexture->texture());
            glBindSampler(textureUnitForDiffuseTex1, _sampler);
			glBindTextureUnit(textureUnitForNormalMap1, _wallNormalMap->texture());
			glBindSampler(textureUnitForNormalMap1, _sampler);
			glBindTextureUnit(textureUnitForDiffuseTex2, _floorTexture->texture());
			glBindSampler(textureUnitForDiffuseTex2, _sampler);
			glBindTextureUnit(textureUnitForNormalMap2, _floorNormalMap->texture());
			glBindSampler(textureUnitForNormalMap2, _sampler);
			for (int i = 0; i < _maze._numPosters; i++) {
				glBindTextureUnit(textureUnitForPosters+i, _posterTextures[i]->texture());
				glBindSampler(textureUnitForPosters+i, _sampler);
			}
        }
        else {
			std::cout << "Not DSA\n";
            glBindSampler(textureUnitForDiffuseTex1, _sampler);
            glActiveTexture(GL_TEXTURE0 + textureUnitForDiffuseTex1);  //текстурный юнит 0
            _wallTexture->bind();

			glBindSampler(textureUnitForNormalMap1, _sampler);
			glActiveTexture(GL_TEXTURE0 + textureUnitForNormalMap1);  //текстурный юнит 1
			_wallNormalMap->bind();

			glBindSampler(textureUnitForDiffuseTex2, _sampler);
			glActiveTexture(GL_TEXTURE0 + textureUnitForDiffuseTex2);  //текстурный юнит 2
			_floorTexture->bind();

			glBindSampler(textureUnitForNormalMap2, _sampler);
			glActiveTexture(GL_TEXTURE0 + textureUnitForNormalMap2);  //текстурный юнит 3
			_floorNormalMap->bind();

			for (int i = 0; i < _maze._numPosters; i++) {
				glBindSampler(textureUnitForPosters + i, _sampler);
				glActiveTexture(GL_TEXTURE0 + textureUnitForPosters + i);  //текстурный юнит 4
				_posterTextures[i]->bind();
			}
        }

        _shader->setIntUniform("diffuseTex", textureUnitForDiffuseTex1);
		_shader->setIntUniform("normalMap", textureUnitForNormalMap1);
		_shader->setVec3Uniform("material.Ka", _wall_mat.Ka);
		_shader->setVec3Uniform("material.Kd", _wall_mat.Kd);
		_shader->setVec3Uniform("material.Ks", _wall_mat.Ks);
		_shader->setFloatUniform("material.shininess", _wall_mat.shininess);
		_shader->setIntUniform("material.haveNormalMap", _wall_mat.haveNormalMap);

		{
			_shader->setMat4Uniform("modelMatrix", _maze._maze->modelMatrix());
			_shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _maze._maze->modelMatrix()))));
			_maze._maze->draw();
		}

		{
			if (_isCeilVisible) {
				_shader->setMat4Uniform("modelMatrix", _maze._ceiling->modelMatrix());
				_shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _maze._ceiling->modelMatrix()))));
				_maze._ceiling->draw();
			}
		}

		_shader->setIntUniform("diffuseTex", textureUnitForDiffuseTex2);
		_shader->setIntUniform("normalMap", textureUnitForNormalMap2);
		_shader->setVec3Uniform("material.Ka", _floor_mat.Ka);
		_shader->setVec3Uniform("material.Kd", _floor_mat.Kd);
		_shader->setVec3Uniform("material.Ks", _floor_mat.Ks);
		_shader->setFloatUniform("material.shininess", _floor_mat.shininess);
		_shader->setIntUniform("material.haveNormalMap", _floor_mat.haveNormalMap);

		{
			_shader->setMat4Uniform("modelMatrix", _maze._floor->modelMatrix());
			_shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _maze._floor->modelMatrix()))));
			_maze._floor->draw();
		}

		_shader->setVec3Uniform("material.Ka", _poster_mat.Ka);
		_shader->setVec3Uniform("material.Kd", _poster_mat.Kd);
		_shader->setVec3Uniform("material.Ks", _poster_mat.Ks);
		_shader->setFloatUniform("material.shininess", _poster_mat.shininess);
		_shader->setIntUniform("material.haveNormalMap", _poster_mat.haveNormalMap);

		{
			for (int i = 0; i < _maze._numPosters; i++) {
				_shader->setIntUniform("diffuseTex", textureUnitForPosters + i);
				_shader->setMat4Uniform("modelMatrix", _maze._posters[i]->modelMatrix());
				_shader->setMat4Uniform("modelMatrix", _maze._posters[i]->modelMatrix());
				_shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _maze._posters[i]->modelMatrix()))));
				_maze._posters[i]->draw();
			}
		}

		//Рисуем маркеры для всех источников света		
		{
			_markerShader->use();
			_markerShader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix * glm::translate(glm::mat4(1.0f), _light.position));
			_markerShader->setVec4Uniform("color", glm::vec4(1.0f, 1.0f, 1.0f, 1.0f));
			_marker->draw();
		}

        //Отсоединяем сэмплер и шейдерную программу
        glBindSampler(0, 0);
        glUseProgram(0);
    }
};

int main()
{
    MazeApplication app;
    app.start();

    return 0;
}