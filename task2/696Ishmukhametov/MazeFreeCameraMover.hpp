#include <Camera.hpp>

#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>
#include <imgui.h>

#include <iostream>
#include "MazeLayout.hpp"
#include <iomanip>

class MazeFreeCameraMover : public CameraMover
{
public:
	MazeFreeCameraMover(glm::vec3& start, MazeLayout& maze) {
		_maze = std::make_shared<MazeLayout>(maze);
		_pos = glm::vec3((start.x+0.5)*_maze->_width, (start.y-1.5)*_maze->_width, (start.z + 0.5)*_maze->_height);
		//��� ����� ���-������ ��������� ��������� ���������� ������
		_rot = glm::toQuat(glm::lookAt(_pos, glm::vec3(_pos.x, start.y*_maze->_width, _pos.z), glm::vec3(0.0f, 0.0f, 1.0f)));
		_speed = 1.5*_maze->_width;
	}
	~MazeFreeCameraMover() {}

	void handleKey(GLFWwindow* window, int key, int scancode, int action, int mods) override;
	void handleMouseMove(GLFWwindow* window, double xpos, double ypos) override;
	void handleScroll(GLFWwindow* window, double xoffset, double yoffset) override;
	void update(GLFWwindow* window, double dt) override;
	glm::vec3 getPosition() {
		return _pos;
	}

	glm::vec3 _pos;
	glm::quat _rot;

	//��������� ������� ���� �� ���������� �����
	double _oldXPos = 0.0;
	double _oldYPos = 0.0;
	float _speed = 1.0;
	std::shared_ptr<MazeLayout> _maze;
};

void MazeFreeCameraMover::handleKey(GLFWwindow* window, int key, int scancode, int action, int mods)
{
}

void MazeFreeCameraMover::handleMouseMove(GLFWwindow* window, double xpos, double ypos)
{
	int state = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);
	if (state == GLFW_PRESS)
	{
		double dx = xpos - _oldXPos;
		double dy = ypos - _oldYPos;

		//��������� ��������� ������� �����/����        
		glm::vec3 rightDir = glm::vec3(1.0f, 0.0f, 0.0f) * _rot;
		_rot *= glm::angleAxis(static_cast<float>(dy * 0.005), rightDir);

		//��������� ��������� ������� ������ ������������ ���
		glm::vec3 upDir(0.0f, 0.0f, 1.0f);
		_rot *= glm::angleAxis(static_cast<float>(dx * 0.005), upDir);
	}

	_oldXPos = xpos;
	_oldYPos = ypos;
}

void MazeFreeCameraMover::handleScroll(GLFWwindow* window, double xoffset, double yoffset)
{
}

void MazeFreeCameraMover::update(GLFWwindow* window, double dt)
{
	//�������� ������� ����������� "������" � ������� ������� ���������
	glm::vec3 forwDir = glm::vec3(0.0f, 0.0f, -1.0f) * _rot;
	forwDir.z = 0.0f;
	//�������� ������� ����������� "������" � ������� ������� ���������
	glm::vec3 rightDir = glm::vec3(1.0f, 0.0f, 0.0f) * _rot;

	glm::vec3 oldPos = _pos;
	//������� ������ ������/�����
	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
	{
		_pos += forwDir * _speed * static_cast<float>(dt);
	}
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
	{
		_pos -= forwDir * _speed * static_cast<float>(dt);
	}
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
	{
		_pos -= rightDir * _speed * static_cast<float>(dt);
	}
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
	{
		_pos += rightDir * _speed * static_cast<float>(dt);
	}
	//-----------------------------------------
	std::setprecision(10);
	std::pair<float, float> corr_pos = _maze->resolveColision(_pos.x, _pos.y, oldPos.x, oldPos.y);
	std::cout << _pos.x << " " << _pos.y << " " << oldPos.x << " " << oldPos.y <<" "<<corr_pos.first<<" "<<corr_pos.second<<"\n";
	_pos.x = corr_pos.first;
	_pos.y = corr_pos.second;
	//��������� ����������� � ������� ������
	_camera.viewMatrix = glm::toMat4(-_rot) * glm::translate(-_pos);

	//-----------------------------------------

	int width, height;
	glfwGetFramebufferSize(window, &width, &height);

	//��������� ������� �������� �� ������, ���� ������� ���� ����������
	_camera.projMatrix = glm::perspective(glm::radians(45.0f), (float)width / height, _near, _far);
}
