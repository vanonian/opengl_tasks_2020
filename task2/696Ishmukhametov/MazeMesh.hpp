#include <Mesh.hpp>

#include <iostream>
#include <vector>
#include <fstream>
#include <string>

#include "MazeLayout.hpp"

class MazeMeshes {
public:
	MazeMeshes() {}
	~MazeMeshes() {}
	void makeMaze(std::string filename, glm::vec3& start, MazeLayout& maze);

	MeshPtr _maze;
	MeshPtr _floor;
	MeshPtr _ceiling;
	std::vector<MeshPtr> _posters;
	int _numPosters;
};

void MazeMeshes::makeMaze(std::string filename, glm::vec3& start, MazeLayout& maze) {
	std::vector<glm::vec3> maze_vertices;
	std::vector<glm::vec3> maze_normals;
	std::vector<glm::vec3> maze_tangents;
	std::vector<unsigned int> maze_indices;
	std::vector<glm::vec2> maze_texcoords;

	std::vector<glm::vec3> ceil_vertices;
	std::vector<glm::vec3> ceil_normals;
	std::vector<glm::vec3> ceil_tangents;
	std::vector<unsigned int> ceil_indices;
	std::vector<glm::vec2> ceil_texcoords;

	std::vector<glm::vec3> floor_vertices;
	std::vector<glm::vec3> floor_normals;
	std::vector<glm::vec3> floor_tangents;
	std::vector<unsigned int> floor_indices;
	std::vector<glm::vec2> floor_texcoords;

	std::vector<glm::vec3> poster_vertices;
	std::vector<glm::vec3> poster_normals;
	std::vector<glm::vec3> poster_tangents;
	std::vector<unsigned int> poster_indices;
	std::vector<glm::vec2> poster_texcoords;

	std::ifstream f;
	f.open(filename);

	int size;
	float width;

	f >> size;
	f >> width;
	f >> start.x >> start.y >> start.z;

	std::vector<std::vector<bool>> maze_map(size, std::vector<bool>(size));
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {
			bool cell;
			f >> cell;
			maze_map[i][j] = cell;
		}
	}

	f >> _numPosters;

	maze = MazeLayout(size, width, maze_map);

	char c1, c2;
	float x, y, z;

	while (f >> c1 >> c2 >> x >> y >> z)
	{
		if (c1=='m' && c2=='v') 
			maze_vertices.push_back(glm::vec3(x*width, y*width, z*width));
		if (c1 == 'm' && c2 == 'n')
			maze_normals.push_back(glm::vec3(x, y, z));
		if (c1 == 'm' && c2 == 't')
			maze_tangents.push_back(glm::vec3(x, y, z));
		if (c1 == 'm' && c2 == 'x')
			maze_texcoords.push_back(glm::vec2(x, y));
		if (c1 == 'm' && c2 == 'f') {
			maze_indices.push_back(unsigned int(x));
			maze_indices.push_back(unsigned int(y));
			maze_indices.push_back(unsigned int(z));
		}

		if (c1 == 'c' && c2 == 'v')
			ceil_vertices.push_back(glm::vec3(x*width, y*width, z*width));
		if (c1 == 'c' && c2 == 'n')
			ceil_normals.push_back(glm::vec3(x, y, z));
		if (c1 == 'c' && c2 == 't')
			ceil_tangents.push_back(glm::vec3(x, y, z));
		if (c1 == 'c' && c2 == 'x')
			ceil_texcoords.push_back(glm::vec2(x, y));
		if (c1 == 'c' && c2 == 'f') {
			ceil_indices.push_back(unsigned int(x));
			ceil_indices.push_back(unsigned int(y));
			ceil_indices.push_back(unsigned int(z));
		}

		if (c1 == 'f' && c2 == 'v')
			floor_vertices.push_back(glm::vec3(x*width, y*width, z*width));
		if (c1 == 'f' && c2 == 'n')
			floor_normals.push_back(glm::vec3(x, y, z));
		if (c1 == 'f' && c2 == 't')
			floor_tangents.push_back(glm::vec3(x, y, z));
		if (c1 == 'f' && c2 == 'x')
			floor_texcoords.push_back(glm::vec2(x, y));
		if (c1 == 'f' && c2 == 'f') {
			floor_indices.push_back(unsigned int(x));
			floor_indices.push_back(unsigned int(y));
			floor_indices.push_back(unsigned int(z));
		}

		if (c1 == 'p' && c2 == 'v')
			poster_vertices.push_back(glm::vec3(x*width, y*width, z*width));
		if (c1 == 'p' && c2 == 'n')
			poster_normals.push_back(glm::vec3(x, y, z));
		if (c1 == 'p' && c2 == 't')
			poster_tangents.push_back(glm::vec3(x, y, z));
		if (c1 == 'p' && c2 == 'x')
			poster_texcoords.push_back(glm::vec2(x, y));
		if (c1 == 'p' && c2 == 'f') {
			poster_indices.push_back(unsigned int(x));
			poster_indices.push_back(unsigned int(y));
			poster_indices.push_back(unsigned int(z));
		}
	}

	f.close();

	//----------------------------------------

	DataBufferPtr maze_vert = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	maze_vert->setData(maze_vertices.size() * sizeof(float) * 3, maze_vertices.data());

	DataBufferPtr maze_norm = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	maze_norm->setData(maze_normals.size() * sizeof(float) * 3, maze_normals.data());

	DataBufferPtr maze_tang = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	maze_tang->setData(maze_tangents.size() * sizeof(float) * 3, maze_tangents.data());

	DataBufferPtr maze_tex = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	maze_tex->setData(maze_texcoords.size() * sizeof(float) * 2, maze_texcoords.data());

	DataBufferPtr maze_idx = std::make_shared<DataBuffer>(GL_ELEMENT_ARRAY_BUFFER);
	maze_idx->setData(maze_indices.size() * sizeof(unsigned int), maze_indices.data());

	_maze = std::make_shared<Mesh>();
	_maze->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, maze_vert);
	_maze->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, maze_norm);
	_maze->setAttribute(2, 3, GL_FLOAT, GL_FALSE, 0, 0, maze_tang);
	_maze->setAttribute(3, 2, GL_FLOAT, GL_FALSE, 0, 0, maze_tex);
	_maze->setIndices(maze_indices.size(), maze_idx);
	_maze->setPrimitiveType(GL_TRIANGLES);
	_maze->setVertexCount(maze_vertices.size());


	DataBufferPtr ceil_vert = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	ceil_vert->setData(ceil_vertices.size() * sizeof(float) * 3, ceil_vertices.data());

	DataBufferPtr ceil_norm = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	ceil_norm->setData(ceil_normals.size() * sizeof(float) * 3, ceil_normals.data());

	DataBufferPtr ceil_tang = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	ceil_tang->setData(ceil_tangents.size() * sizeof(float) * 3, ceil_tangents.data());

	DataBufferPtr ceil_tex = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	ceil_tex->setData(ceil_texcoords.size() * sizeof(float) * 2, ceil_texcoords.data());


	DataBufferPtr ceil_idx = std::make_shared<DataBuffer>(GL_ELEMENT_ARRAY_BUFFER);
	ceil_idx->setData(ceil_indices.size() * sizeof(unsigned int), ceil_indices.data());

	_ceiling = std::make_shared<Mesh>();
	_ceiling->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, ceil_vert);
	_ceiling->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, ceil_norm);
	_ceiling->setAttribute(2, 3, GL_FLOAT, GL_FALSE, 0, 0, ceil_tang);
	_ceiling->setAttribute(3, 2, GL_FLOAT, GL_FALSE, 0, 0, ceil_tex);
	_ceiling->setIndices(ceil_indices.size(), ceil_idx);
	_ceiling->setPrimitiveType(GL_TRIANGLES);
	_ceiling->setVertexCount(ceil_vertices.size());

	DataBufferPtr floor_vert = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	floor_vert->setData(floor_vertices.size() * sizeof(float) * 3, floor_vertices.data());

	DataBufferPtr floor_norm = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	floor_norm->setData(floor_normals.size() * sizeof(float) * 3, floor_normals.data());

	DataBufferPtr floor_tang = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	floor_tang->setData(floor_tangents.size() * sizeof(float) * 3, floor_tangents.data());

	DataBufferPtr floor_tex = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	floor_tex->setData(floor_texcoords.size() * sizeof(float) * 2, floor_texcoords.data());


	DataBufferPtr floor_idx = std::make_shared<DataBuffer>(GL_ELEMENT_ARRAY_BUFFER);
	floor_idx->setData(floor_indices.size() * sizeof(unsigned int), floor_indices.data());

	_floor = std::make_shared<Mesh>();
	_floor->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, floor_vert);
	_floor->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, floor_norm);
	_floor->setAttribute(2, 3, GL_FLOAT, GL_FALSE, 0, 0, floor_tang);
	_floor->setAttribute(3, 2, GL_FLOAT, GL_FALSE, 0, 0, floor_tex);
	_floor->setIndices(floor_indices.size(), floor_idx);
	_floor->setPrimitiveType(GL_TRIANGLES);
	_floor->setVertexCount(floor_vertices.size());

	_posters = std::vector<MeshPtr>(_numPosters);
	for (int i=0; i < _numPosters; i++) {
		_posters[i] = std::make_shared<Mesh>();
	}
	for (int i = 0; i < _numPosters; i++) {
		std::vector<glm::vec3> p_vertices;
		std::vector<glm::vec3> p_normals;
		std::vector<glm::vec3> p_tangents;
		std::vector<unsigned int> p_indices;
		std::vector<glm::vec2> p_texcoords;

		for (int j = 0; j < poster_vertices.size() / _numPosters; j++) {
			p_vertices.push_back(poster_vertices[i*poster_vertices.size() / _numPosters + j]);
			p_normals.push_back(poster_normals[i*poster_vertices.size() / _numPosters + j]);
			p_tangents.push_back(poster_tangents[i*poster_vertices.size() / _numPosters + j]);
			p_texcoords.push_back(poster_texcoords[i*poster_vertices.size() / _numPosters + j]);
		}
		for (int j = 0; j < poster_indices.size() / _numPosters; j++) {
			p_indices.push_back(poster_indices[i*poster_indices.size() / _numPosters + j]);
		}

		DataBufferPtr poster_vert = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
		poster_vert->setData(p_vertices.size() * sizeof(float) * 3, p_vertices.data());

		DataBufferPtr poster_norm = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
		poster_norm->setData(p_normals.size() * sizeof(float) * 3, p_normals.data());

		DataBufferPtr poster_tang = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
		poster_tang->setData(p_tangents.size() * sizeof(float) * 3, p_tangents.data());

		DataBufferPtr poster_tex = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
		poster_tex->setData(p_texcoords.size() * sizeof(float) * 2, p_texcoords.data());

		DataBufferPtr poster_idx = std::make_shared<DataBuffer>(GL_ELEMENT_ARRAY_BUFFER);
		poster_idx->setData(p_indices.size() * sizeof(unsigned int), p_indices.data());

		_posters[i]->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, poster_vert);
		_posters[i]->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, poster_norm);
		_posters[i]->setAttribute(2, 3, GL_FLOAT, GL_FALSE, 0, 0, poster_tang);
		_posters[i]->setAttribute(3, 2, GL_FLOAT, GL_FALSE, 0, 0, poster_tex);
		_posters[i]->setIndices(p_indices.size(), poster_idx);
		_posters[i]->setPrimitiveType(GL_TRIANGLES);
		_posters[i]->setVertexCount(p_vertices.size());
		std::cout << "Poster " << i << " is created with " << p_vertices.size() << " vertices\n";
	}


	std::cout << "Maze is created with " << maze_vertices.size() << " vertices\n";
	std::cout << "Ceiling is created with " << ceil_vertices.size() << " vertices\n";
	std::cout << "Floor is created with " << floor_vertices.size() << " vertices\n";
}