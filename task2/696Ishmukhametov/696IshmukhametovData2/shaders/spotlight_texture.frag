#version 330

uniform sampler2D diffuseTex;
uniform sampler2D normalMap;

struct LightInfo
{
	vec3 pos; //��������� ��������� ����� � ������� ��������� ����������� ������!
	vec3 La; //���� � ������������� ����������� �����
	vec3 Ld; //���� � ������������� ���������� �����
	vec3 Ls; //���� � ������������� ��������� �����
	vec3 a;
};

struct MaterialInfo
{
	vec3 Ka; //����������� ��������� ����������� �����
	vec3 Kd; //����������� ��������� ���������� �����
	vec3 Ks; //����������� ��������� ���������
	float shininess;
	bool haveNormalMap;
};


uniform LightInfo light;
uniform MaterialInfo material;

in mat3 tbn;
in vec4 posCamSpace; //���������� ������� � ������� ��������� ������ (��������������� ����� ��������� ������������)
in vec2 texCoord; //���������� ���������� (��������������� ����� ��������� ������������)

out vec4 fragColor; //�������� ���� ���������

const float sqr_max_ang_cos = 0.85;
void main()
{
	vec3 normal;
	vec3 diffuseColor = texture(diffuseTex, texCoord).rgb;
	vec3 color = diffuseColor * material.Ka*light.La;

	vec3 viewDirection = normalize(-posCamSpace.xyz); //����������� �� ����������� ������ (��� ��������� � ����� (0.0, 0.0, 0.0))
	vec3 lightDirCamSpace = normalize(light.pos - posCamSpace.xyz); //����������� �� �������� �����	
	
	vec3 lightDir = vec3(0, -0.25,-1);
	float lightDirSqrLen = pow(lightDir.x, 2)+pow(lightDir.y,2)+pow(lightDir.z,2);
	float lightDirCamSpaceSqrLen = pow(lightDirCamSpace.x, 2)+pow(lightDirCamSpace.y,2)+pow(lightDirCamSpace.z,2);
	float SqrLdotLC = pow(dot(lightDir, lightDirCamSpace), 2);
	float MaxSqrLdotLC = lightDirSqrLen*lightDirCamSpaceSqrLen;
	if (SqrLdotLC >= sqr_max_ang_cos*MaxSqrLdotLC) {
	
	float Kf = 1.5*(SqrLdotLC/MaxSqrLdotLC - sqr_max_ang_cos)/(1-sqr_max_ang_cos); //����������� ��������� ��������

		if (material.haveNormalMap) {
			vec3 normalFromTex = normalize(2*(texture(normalMap, texCoord).rgb-0.5));
			normal = normalize(tbn*normalFromTex);
		} else {
			normal = normalize(tbn*vec3(0,0,1));
		}

		float dd = pow(posCamSpace.x-light.pos.x,2)+pow(posCamSpace.y-light.pos.y,2)+pow(posCamSpace.z-light.pos.z,2);
		float d = sqrt(dd);
		float fading = light.a.x + light.a.y*d + light.a.z*dd;

		float NdotL = max(dot(normal, lightDirCamSpace.xyz), 0.0); //��������� ������������ (�������)
		color += Kf*diffuseColor * material.Kd*light.Ld * NdotL/fading;

		if (NdotL > 0.0)
		{			
			vec3 halfVector = normalize(lightDirCamSpace.xyz + viewDirection); //����������� ����� ������������� �� ������ � �� �������� �����

			float blinnTerm = max(dot(normal, halfVector), 0.0); //������������� ��������� ��������� �� ������				
			blinnTerm = pow(blinnTerm, material.shininess); //���������� ������ �����
			color += Kf*material.Ks*light.Ls * blinnTerm/fading;
		}
	}
		fragColor = vec4(color, 1.0);
}
