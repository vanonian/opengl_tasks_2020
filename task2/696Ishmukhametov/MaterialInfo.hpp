#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

/**
��������� ��������� �������
*/
struct MaterialInfo
{
	///����������� ��������� ����������� �����
	glm::vec3 Ka;

	///����������� ��������� ���������� �����
	glm::vec3 Kd;

	///����������� ��������� ��������� �����
	glm::vec3 Ks;

	///����������� ���� �����
	float shininess;

	///���� ������� ����� ��������
	bool haveNormalMap;

	MaterialInfo() : haveNormalMap(false) {}
};