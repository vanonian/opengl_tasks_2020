#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include <Texture.hpp>

#include "MazeLayout.hpp"
#include "MazeMesh.hpp"
#include "MazeFreeCameraMover.hpp"
#include "ObjectInfo/MazeMaterialInfo.hpp"
#include "ObjectInfo/MazeLightInfo.hpp"

#include <iostream>
#include <sstream>
#include <vector>
#include <memory>


/**
Пример с текстурированием разных 3д-моделей
*/
class MazeApplication : public Application
{
public:

	MazeApplication() {
		_cameraMover = std::make_shared<OrbitCameraMover>();
	}

	//------------------------------------------------------
	//------------------------------------------------------

	MeshPtr _marker;

	MazeMeshes _maze;
	MazeLayout maze;

	//------------------------------------------------------
	//------------------------------------------------------

	CameraMoverPtr _orbitCameraMover;
	CameraMoverPtr _freeCameraMover;
	CameraInfo _mapCamera;

	//------------------------------------------------------
	//------------------------------------------------------

	ShaderProgramPtr _markerShader;

	ShaderProgramPtr _pointlight_shader;
	ShaderProgramPtr _spotlight_shader;
	ShaderProgramPtr _shader = _pointlight_shader;

	ShaderProgramPtr _map_shader;

	//------------------------------------------------------
	//------------------------------------------------------

	TexturePtr _wallTexture;
	TexturePtr _wallNormalMap;
	TexturePtr _wallMapTexture;

	TexturePtr _floorTexture;
	TexturePtr _floorNormalMap;
	TexturePtr _floorMapTexture;

	std::vector<TexturePtr> _portalTextures;
	std::vector<TexturePtr> _posterTextures;

	//------------------------------------------------------
	//------------------------------------------------------

	GLuint _sampler;

	//------------------------------------------------------
	//------------------------------------------------------

	std::vector<GLuint> _portal_framebuffer = std::vector<GLuint>(3);
	std::vector<GLuint> _portal_renderTex = std::vector<GLuint>(3);

	unsigned int _fbWidth;
	unsigned int _fbHeight;
	
	std::vector<CameraInfo> _portal_fbCamera = std::vector<CameraInfo>(2);

	//------------------------------------------------------
	//------------------------------------------------------

	float _orb_lr = 20.0;
	float _orb_phi = 0.0;
	float _orb_theta = glm::pi<float>() * 0.25f;

	//------------------------------------------------------
	//------------------------------------------------------

	MazeLightInfo _orb_light;
	MazeLightInfo _head_light;
	MazeLightInfo& _light = _orb_light;

	bool _isHeadLight = false;

	//------------------------------------------------------
	//------------------------------------------------------

	MazeMaterialInfo _wall_material;
	MazeMaterialInfo _floor_material;
	MazeMaterialInfo _poster_material;

	//------------------------------------------------------
	//------------------------------------------------------

	void handleKey(int key, int scancode, int action, int mods) override;
	void makeScene() override;
	void updateGUI() override;

	//------------------------------------------------------
	//------------------------------------------------------

	void initFramebuffer_DSA(GLuint& framebuffer, GLuint& texture);
	void initFramebuffer_noDSA(GLuint& framebuffer, GLuint& texture);
	void initFramebuffer(GLuint& framebuffer, GLuint& texture);

	//------------------------------------------------------
	//------------------------------------------------------

	void draw() override;
	void drawScene(CameraInfo& camera, bool is_portals_placed);
	void drawToFramebuffer(CameraInfo& camera, GLuint& framebuffer);
	void drawMesh(ShaderProgramPtr& shader, CameraInfo& camera, MeshPtr& mesh,
				  TexturePtr& texture, TexturePtr& normalMap);
	void drawPortals(ShaderProgramPtr& shader, CameraInfo& camera, 
					 std::vector<TexturePtr>& textures, bool is_portals_placed);
	void drawMap(CameraInfo& camera);
};

//------------------------------------------------------
//------------------------------------------------------

void MazeApplication::handleKey(int key, int scancode, int action, int mods) {
	if (action == GLFW_PRESS) {
		if (key == GLFW_KEY_C) {
			_maze._isCeilVIsible = !_maze._isCeilVIsible;
		}
		else if (key == GLFW_KEY_Q) {
			_cameraMover = _isHeadLight ? _orbitCameraMover : _freeCameraMover;
			_isHeadLight = !_isHeadLight;
		}
		else if (key == GLFW_KEY_1) {
			_maze.portalUpdate(0, _cameraMover, maze, _isHeadLight, _freeCameraMover);
		}
		else if (key == GLFW_KEY_2) {
			_maze.portalUpdate(1, _cameraMover, maze, _isHeadLight, _freeCameraMover);
		}
		else {
			Application::handleKey(key, scancode, action, mods);
		}
	}
}

void MazeApplication::updateGUI()
{
	Application::updateGUI();

	ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
	if (ImGui::Begin("Light position", NULL, ImGuiWindowFlags_AlwaysAutoResize))
	{
		ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

		if (ImGui::CollapsingHeader("Light"))
		{
			ImGui::ColorEdit3("ambient", glm::value_ptr(_light.ambient));
			ImGui::ColorEdit3("diffuse", glm::value_ptr(_light.diffuse));
			ImGui::ColorEdit3("specular", glm::value_ptr(_light.specular));

			ImGui::SliderFloat("radius", &_orb_lr, 0.1f, 40.0f);
			ImGui::SliderFloat("phi", &_orb_phi, 0.0f, 2.0f * glm::pi<float>());
			ImGui::SliderFloat("theta", &_orb_theta, 0.0f, glm::pi<float>());
		}
	}
	ImGui::End();
}

//------------------------------------------------------
//------------------------------------------------------

void MazeApplication::makeScene()
{
	Application::makeScene();

	//------------------------------------------------------
	//------------------------------------------------------

	glm::vec3 maze_start;
	std::vector<std::string> paths = { "696IshmukhametovData3/structure/maze.txt",
									   "696IshmukhametovData3/structure/walls.txt",
									   "696IshmukhametovData3/structure/ceiling.txt",
									   "696IshmukhametovData3/structure/floor.txt",
									   "696IshmukhametovData3/structure/posters.txt",
									   "696IshmukhametovData3/structure/portals.txt" };
	_maze.makeMaze(paths, maze_start, maze);


	_maze._maze->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));
	_maze._ceiling->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));
	_maze._floor->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));

	for (int i = 0; i < _maze._numPosters; i++) {
		_maze._posters[i]->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));
	}

	//------------------------------------------------------
	//------------------------------------------------------

	_marker = makeSphere(0.1f);

	//------------------------------------------------------
	//------------------------------------------------------

	_orbitCameraMover = std::make_shared<OrbitCameraMover>();
	_freeCameraMover = std::make_shared<MazeFreeCameraMover>(maze_start, maze);

	_mapCamera.viewMatrix = glm::lookAt(glm::vec3(maze._center, maze._center, 5.0f), 
										glm::vec3(maze._center, maze._center, 0.0f),
										glm::vec3(0.0f, 1.0f, 0.0f));
	_mapCamera.projMatrix = glm::ortho(-1.2f*maze._center, 1.2f*maze._center,
									   -1.2f*maze._center, 1.2f*maze._center,
										0.1f, 100.0f);

	//------------------------------------------------------
	//------------------------------------------------------

	_pointlight_shader = std::make_shared<ShaderProgram>("696IshmukhametovData3/shaders/texture.vert", 
														"696IshmukhametovData3/shaders/pointlight_texture.frag");
	_spotlight_shader = std::make_shared<ShaderProgram>("696IshmukhametovData3/shaders/texture.vert", 
														"696IshmukhametovData3/shaders/spotlight_texture.frag");
	_markerShader = std::make_shared<ShaderProgram>("696IshmukhametovData3/shaders/marker.vert", 
													"696IshmukhametovData3/shaders/marker.frag");
	_map_shader = std::make_shared<ShaderProgram>("696IshmukhametovData3/shaders/map.vert", 
												  "696IshmukhametovData3/shaders/map.frag");

	//------------------------------------------------------
	//------------------------------------------------------

	_orb_light.position = glm::vec3(glm::cos(_orb_phi) * glm::cos(_orb_theta), 
									glm::sin(_orb_phi) * glm::cos(_orb_theta), 
									glm::sin(_orb_theta)) * _orb_lr;

	_orb_light.setLightParams(glm::vec3(0.3, 0.3, 0.3), 
				   glm::vec3(0.7, 0.7, 0.7), glm::vec3(1.0, 1.0, 1.0), 
				   glm::vec3(1.0, 0.01, 0.00004));

	//------------------------------------------------------
	//------------------------------------------------------

	_head_light.position = std::static_pointer_cast<MazeFreeCameraMover>(_freeCameraMover)->getPosition() + 
						   glm::vec3(0.0, 0.0, 0.25);

	_head_light.setLightParams(glm::vec3(0.3, 0.3, 0.3),
		glm::vec3(0.7, 0.7, 0.7), glm::vec3(1.0, 1.0, 1.0),
		glm::vec3(1.0, 0.02, 0.005));

	//------------------------------------------------------
	//------------------------------------------------------

	_wall_material.setMaterialParams(glm::vec3(1.0, 1.0, 1.0),
		glm::vec3(0.9, 0.9, 0.9), glm::vec3(0.1, 0.1, 0.1),
		16.0, true);

	_floor_material.setMaterialParams(glm::vec3(1.0, 1.0, 1.0),
		glm::vec3(0.8, 0.8, 0.8), glm::vec3(0.5, 0.5, 0.5),
		128.0, true);

	_poster_material.setMaterialParams(glm::vec3(1.0, 1.0, 1.0),
		glm::vec3(0.8, 0.8, 0.8), glm::vec3(0.2, 0.2, 0.2),
		32.0, false);

	//------------------------------------------------------
	//------------------------------------------------------

	_wallTexture = loadTexture("696IshmukhametovData3/textures/wall/texture.jpg");
	_wallNormalMap = loadTexture("696IshmukhametovData3/textures/wall/normal_map.jpg");
	_wallMapTexture = loadTexture("696IshmukhametovData3/textures/map/wall.jpg");

	_floorTexture = loadTexture("696IshmukhametovData3/textures/floor/texture.jpg");
	_floorNormalMap = loadTexture("696IshmukhametovData3/textures/floor/normal_map.jpg");
	_floorMapTexture = loadTexture("696IshmukhametovData3/textures/map/floor.jpg");

	_portalTextures = std::vector<TexturePtr>(2);
	_portalTextures[0] = loadTexture("696IshmukhametovData3/textures/portals/blue_portal.jpg");
	_portalTextures[1] = loadTexture("696IshmukhametovData3/textures/portals/orange_portal.jpg");

	_posterTextures = std::vector<TexturePtr>(_maze._numPosters);
	for (int i = 0; i < _maze._numPosters; i++) {
		_posterTextures[i] = loadTexture("696IshmukhametovData3/textures/posters/poster" + std::to_string(i) + ".jpg");
	}

	//------------------------------------------------------
	//------------------------------------------------------

	glGenSamplers(1, &_sampler);
	glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glSamplerParameterf(_sampler, GL_TEXTURE_MAX_ANISOTROPY_EXT, static_cast<GLfloat>(8));

	//------------------------------------------------------
	//------------------------------------------------------

	_fbWidth = 512;
	_fbHeight = 1024;
	initFramebuffer(_portal_framebuffer[0], _portal_renderTex[0]);
	initFramebuffer(_portal_framebuffer[1], _portal_renderTex[1]);
	initFramebuffer(_portal_framebuffer[2], _portal_renderTex[2]);
}

//------------------------------------------------------
//------------------------------------------------------

void MazeApplication::initFramebuffer_DSA(GLuint& framebuffer, GLuint& texture) {
	// Создаём буфер кадра.
	glCreateFramebuffers(1, &framebuffer);
	// Создаём текстуру, куда будет осуществляться рендеринг.
	glCreateTextures(GL_TEXTURE_2D, 1, &texture);
	// Аллоцируем место для текстуры. Mip-уровни считать не будем, поэтому всего 1 уровень.
	glTextureStorage2D(texture, 1, GL_RGB8, _fbWidth, _fbHeight);

	// Привяжем текстуру к буферу кадра к 0-й точке привязке цвета.
	glNamedFramebufferTexture(framebuffer, GL_COLOR_ATTACHMENT0, texture, 0);

	// Создадим текстуру для глубины и привяжем.
	GLuint depthTex;
	glCreateTextures(GL_TEXTURE_2D, 1, &depthTex);
	glTextureStorage2D(depthTex, 1, GL_DEPTH24_STENCIL8, _fbWidth, _fbHeight);
	glNamedFramebufferTexture(framebuffer, GL_DEPTH_STENCIL_ATTACHMENT, depthTex, 0);

	// Выходная переменная фрагментного шейдера пойдет в изображение, привязанное к этой точке привязки.
	glNamedFramebufferDrawBuffer(framebuffer, GL_COLOR_ATTACHMENT0);

	// Проверяем, что в буфер кадра можно рендерить.
	// В принципе, если этого не сделать, ничего криминального не произойдет - debug context output (если подключен) выдаст сообщение, если произошла попытка рендеринга в невалидный буфер кадра.
	assert(glCheckNamedFramebufferStatus(framebuffer, GL_DRAW_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE);
}

void MazeApplication::initFramebuffer_noDSA(GLuint& framebuffer, GLuint& texture) {
	//------------------------------------------------------
	//------------------------------------------------------

	//Создаем фреймбуфер
	glGenFramebuffers(1, &framebuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);

	//------------------------------------------------------
	//------------------------------------------------------

	//Создаем текстуру, куда будет осуществляться рендеринг
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, _fbWidth, _fbHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);
	glGenerateMipmap(GL_TEXTURE_2D);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture, 0);

	//------------------------------------------------------
	//------------------------------------------------------

	//Создаем буфер глубины для фреймбуфера
	GLuint depthRenderBuffer;
	glGenRenderbuffers(1, &depthRenderBuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, _fbWidth, _fbHeight);

	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);

	//------------------------------------------------------
	//------------------------------------------------------

	//Указываем куда именно мы будем рендерить
	GLenum buffers[] = { GL_COLOR_ATTACHMENT0 };
	glDrawBuffers(1, buffers);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		std::cerr << "Failed to setup framebuffer\n";
		exit(1);
	}
	glBindTexture(GL_TEXTURE_2D, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void MazeApplication::initFramebuffer(GLuint& framebuffer, GLuint& texture)
{
	if (USE_DSA)
		initFramebuffer_DSA(framebuffer, texture);
	else
		initFramebuffer_noDSA(framebuffer, texture);
}

void MazeApplication::draw()
{
	_orb_light.position = glm::vec3(glm::cos(_orb_phi) * glm::cos(_orb_theta),
		glm::sin(_orb_phi) * glm::cos(_orb_theta),
		glm::sin(_orb_theta)) * _orb_lr;
	_head_light.position = std::static_pointer_cast<MazeFreeCameraMover>(_freeCameraMover)->getPosition() +
		glm::vec3(0.0, 0.0, 0.25);

	//------------------------------------------------------
	//------------------------------------------------------

	_light = _isHeadLight ? _head_light : _orb_light;
	_shader = _isHeadLight ? _spotlight_shader : _pointlight_shader;

	//------------------------------------------------------
	//------------------------------------------------------

	if (_maze._is_portal_placed[0] && _maze._is_portal_placed[1]) {
		for (int i = 0; i < 2; i++) {
		
			glm::vec4 pos = _camera.viewMatrix[3];

			int face1 = _maze._portal_positioned[1 - i];
			int face2 = _maze._portal_positioned[i];

			glm::mat4 todo;
			glm::mat4 dodo;

			if (face1 == 0) {

				todo = glm::translate(glm::mat4(1.0), glm::vec3(-0.5f*maze._width, 0.0f, -maze._width));
			}
			if (face1 == 1) {

				todo = glm::rotate(glm::mat4(1.0), glm::radians(180.f), glm::vec3(0.0f, 0.0f, 1.0f))*
					glm::translate(glm::mat4(1.0), glm::vec3(-0.5f*maze._width, 0.0f, -maze._width))*
					glm::translate(glm::mat4(1.0), glm::vec3(0.0f, -maze._width, 0.0f));
			}
			if (face1 == 2) {

				todo = glm::rotate(glm::mat4(1.0), glm::radians(180.f), glm::vec3(0.0f, 0.0f, 1.0f))*
					   glm::translate(glm::mat4(1.0), glm::vec3(-0.5f*maze._width, 0.0f, -maze._width))*
					   glm::rotate(glm::mat4(1.0), glm::radians(-90.f), glm::vec3(0.0f, 0.0f, 1.0f));
			}
			if (face1 == 3) {

				todo = glm::translate(glm::mat4(1.0), glm::vec3(-0.5f*maze._width, 0.0f, -maze._width))*
					   glm::rotate(glm::mat4(1.0), glm::radians(-90.f), glm::vec3(0.0f, 0.0f, 1.0f))*
					   glm::translate(glm::mat4(1.0), glm::vec3(-maze._width, 0.0f, 0.0f));
				}
			if (face2 == 0) {

				dodo = glm::mat4(1.0);
			}
			if (face2 == 1) {

				dodo = glm::rotate(glm::mat4(1.0), glm::radians(180.f), glm::vec3(0.0f, 0.0f, 1.0f));
			}
			if (face2 == 2) {

				dodo = glm::rotate(glm::mat4(1.0), glm::radians(-90.f), glm::vec3(0.0f, 0.0f, 1.0f));
			}
			if (face2 == 3) {

				dodo = glm::rotate(glm::mat4(1.0), glm::radians(90.f), glm::vec3(0.0f, 0.0f, 1.0f));
			}

			_portal_fbCamera[i].viewMatrix = (glm::translate(glm::mat4(1.0), glm::vec3(-pos.x, -pos.y, -pos.z))*_camera.viewMatrix)*
											glm::rotate(glm::mat4(1.0),glm::radians(180.f),glm::vec3(0.0f,0.0f,1.0f))*
											dodo*
											todo*
											glm::inverse(_maze._portals[1 - i][_maze._portal_positioned[1 - i]]->modelMatrix());

			_portal_fbCamera[i].projMatrix = glm::perspective(glm::radians(30.0f), (float)_fbWidth / _fbHeight, 0.1f, 100.0f);

			drawToFramebuffer(_portal_fbCamera[i], _portal_framebuffer[i]);
		}

		drawScene(_camera, true);
	}
	else {
		drawScene(_camera, false);
	}

	glBindSampler(0, 0);
	glUseProgram(0);
}

void MazeApplication::drawMesh(ShaderProgramPtr& shader, CameraInfo& camera, MeshPtr& mesh, 
							   TexturePtr& texture, TexturePtr& normalMap) {

	if (USE_DSA) {
		glBindTextureUnit(0, texture->texture());
		glBindSampler(0, _sampler);

		glBindTextureUnit(1, normalMap->texture());
		glBindSampler(1, _sampler);
	}
	else {
		glBindSampler(0, _sampler);
		glActiveTexture(GL_TEXTURE0 + 0);
		texture->bind();

		glBindSampler(1, _sampler);
		glActiveTexture(GL_TEXTURE0 + 1);
		normalMap->bind();
	}

	//------------------------------------------------------
	//------------------------------------------------------

	shader->setIntUniform("diffuseTex", 0);
	shader->setIntUniform("normalMap", 1);
	{
		shader->setMat4Uniform("modelMatrix", mesh->modelMatrix());
		shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(camera.viewMatrix * mesh->modelMatrix()))));
		mesh->draw();
	}
}


void MazeApplication::drawPortals(ShaderProgramPtr& shader, CameraInfo& camera, 
								  std::vector<TexturePtr>& textures, bool is_portals_placed) {
	if (is_portals_placed) {
		glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
		glBindSampler(0, _sampler);
		glBindTexture(GL_TEXTURE_2D, _portal_renderTex[0]);
		glGenerateMipmap(GL_TEXTURE_2D);

		glActiveTexture(GL_TEXTURE1);  //текстурный юнит 1
		glBindSampler(1, _sampler);
		glBindTexture(GL_TEXTURE_2D, _portal_renderTex[1]);
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else {
		if (USE_DSA) {
			glBindTextureUnit(0, textures[0]->texture());
			glBindSampler(0, _sampler);

			glBindTextureUnit(1, textures[1]->texture());
			glBindSampler(1, _sampler);
		}
		else {
			glBindSampler(0, _sampler);
			glActiveTexture(GL_TEXTURE0 + 0);
			textures[0]->bind();

			glBindSampler(1, _sampler);
			glActiveTexture(GL_TEXTURE0 + 1);
			textures[1]->bind();
		}
	}

	//------------------------------------------------------
	//------------------------------------------------------

	shader->use();
	shader->setMat4Uniform("viewMatrix", camera.viewMatrix);
	shader->setMat4Uniform("projectionMatrix", camera.projMatrix);

	//------------------------------------------------------
	//------------------------------------------------------

	glEnable(GL_STENCIL_TEST);
	glStencilFunc(GL_ALWAYS, 1.0f, 1.0f);
	glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);

	for (int i = 0; i < 2; i++) {
		shader->setIntUniform("diffuseTex", i);

		if (_maze._is_portal_placed[i]) {
			{
				if (!is_portals_placed) {

					glEnable(GL_BLEND);
					glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
				}

				shader->setMat4Uniform("modelMatrix", _maze._portals[i][_maze._portal_positioned[i]]->modelMatrix()
													  *glm::translate(glm::mat4(1.0f), glm::vec3(0.05*(1 - _maze._portal_positioned[i] / 2), 0.05*(_maze._portal_positioned[i] / 2), 0.05*maze._width))
													  *glm::scale(glm::vec3(1.0-0.1*(1-_maze._portal_positioned[i] / 2), 1.0-0.1*(_maze._portal_positioned[i] / 2), 0.94)));
				_maze._portals[i][_maze._portal_positioned[i]]->draw();

				glDisable(GL_BLEND);
			}
		}
		else if (_maze._is_portal_processed[i]) {
			_maze.findPortalPos(i, _freeCameraMover, maze, _isHeadLight);
			
			if (_maze._portal_positioned[i] != -1) {
				glEnable(GL_BLEND);
				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

				{
					shader->setMat4Uniform("modelMatrix", _maze._portals[i][_maze._portal_positioned[i]]->modelMatrix()
						*glm::translate(glm::mat4(1.0f), glm::vec3(0.05*(1 - _maze._portal_positioned[i] / 2), 0.05*(_maze._portal_positioned[i] / 2), 0.08*maze._width))
						*glm::scale(glm::vec3(1.0 - 0.1*(1-_maze._portal_positioned[i] / 2), 1.0 - 0.1*(_maze._portal_positioned[i] / 2), 0.9)));
					_maze._portals[i][_maze._portal_positioned[i]]->draw();
				}

				glDisable(GL_BLEND);
			}
		}
	}

	if (USE_DSA) {
		glBindTextureUnit(0, textures[0]->texture());
		glBindSampler(0, _sampler);

		glBindTextureUnit(1, textures[1]->texture());
		glBindSampler(1, _sampler);
	}
	else {
		glBindSampler(0, _sampler);
		glActiveTexture(GL_TEXTURE0 + 0);
		textures[0]->bind();

		glBindSampler(1, _sampler);
		glActiveTexture(GL_TEXTURE0 + 1);
		textures[1]->bind();
	}

	glStencilFunc(GL_NOTEQUAL, 1.0f, 1.0f);
	glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);

	for (int i = 0; i < 2; i++) {
		shader->setIntUniform("diffuseTex", i);

		if (_maze._is_portal_placed[i]) {

			{
				shader->setMat4Uniform("modelMatrix", _maze._portals[i][_maze._portal_positioned[i]]->modelMatrix());
				_maze._portals[i][_maze._portal_positioned[i]]->draw();
			}
		}
		else if (_maze._is_portal_processed[i]) {
			if (_maze._portal_positioned[i] != -1) {

				glEnable(GL_BLEND);
				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

				{
					shader->setMat4Uniform("modelMatrix", _maze._portals[i][_maze._portal_positioned[i]]->modelMatrix());
					_maze._portals[i][_maze._portal_positioned[i]]->draw();
				}

				glDisable(GL_BLEND);
			}
		}
	}

	glDisable(GL_STENCIL_TEST);
}

void MazeApplication::drawMap(CameraInfo& camera) {

	if (USE_DSA) {
		glBindTextureUnit(0, _wallMapTexture->texture());
		glBindSampler(0, _sampler);

		glBindTextureUnit(1, _floorMapTexture->texture());
		glBindSampler(1, _sampler);
	}
	else {
		glBindSampler(0, _sampler);
		glActiveTexture(GL_TEXTURE0 + 0);
		_wallMapTexture->bind();

		glBindSampler(1, _sampler);
		glActiveTexture(GL_TEXTURE0 + 1);
		_floorMapTexture->bind();
	}

	glClearStencil(1);
	glClear(GL_STENCIL_BUFFER_BIT);

	glEnable(GL_STENCIL_TEST);
	glStencilFunc(GL_NEVER, 0.0f, 1.0f);
	glStencilOp(GL_REPLACE, GL_KEEP, GL_REPLACE);

	{
		_markerShader->use();
		_markerShader->setMat4Uniform("mvpMatrix", camera.projMatrix * camera.viewMatrix *
			glm::translate(glm::mat4(1.0f), _head_light.position));
		_markerShader->setMat4Uniform("scaleMatrix", glm::scale(glm::vec3(0.5f*maze._width / 0.1f)));
		_markerShader->setVec4Uniform("color", glm::vec4(1.0f, 1.0f, 1.0f, 1.0f));
		_marker->draw();
	}

	//------------------------------------------------------
	//------------------------------------------------------
	glStencilFunc(GL_NOTEQUAL, 0.0f, 1.0f);
	glStencilOp(GL_INCR, GL_KEEP, GL_KEEP);

	_map_shader->use();

	_map_shader->setMat4Uniform("viewMatrix", camera.viewMatrix);
	_map_shader->setMat4Uniform("projectionMatrix", camera.projMatrix);

	//------------------------------------------------------
	//------------------------------------------------------

	_map_shader->setIntUniform("diffuseTex", 0);
	{
		_map_shader->setMat4Uniform("modelMatrix", _maze._maze->modelMatrix());
		_maze._maze->draw();
	}

	//------------------------------------------------------
	//------------------------------------------------------

	_map_shader->setIntUniform("diffuseTex", 1);
	{
		_map_shader->setMat4Uniform("modelMatrix", _maze._floor->modelMatrix());
		_maze._floor->draw();
	}

	//------------------------------------------------------
	//------------------------------------------------------


	if (USE_DSA) {
		glBindTextureUnit(0, _portalTextures[0]->texture());
		glBindSampler(0, _sampler);

		glBindTextureUnit(1, _portalTextures[1]->texture());
		glBindSampler(1, _sampler);
	}
	else {
		glBindSampler(0, _sampler);
		glActiveTexture(GL_TEXTURE0 + 0);
		_portalTextures[0]->bind();

		glBindSampler(1, _sampler);
		glActiveTexture(GL_TEXTURE0 + 1);
		_portalTextures[1]->bind();
	}

	for (int i = 0; i < 2; i++) {
		if (_maze._is_portal_placed[i]) {
			_map_shader->setIntUniform("diffuseTex", i);
			_map_shader->setMat4Uniform("modelMatrix", _maze._map_portals[i]->modelMatrix());
			_maze._map_portals[i]->draw();
		}
	}

	//------------------------------------------------------
	//------------------------------------------------------
	glStencilFunc(GL_EQUAL, 0.0f, 1.0f);
	glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
	
	{
		_markerShader->use();
		_markerShader->setMat4Uniform("mvpMatrix", camera.projMatrix * camera.viewMatrix *
												   glm::translate(glm::mat4(1.0f), _head_light.position));
		_markerShader->setMat4Uniform("scaleMatrix", glm::scale(glm::vec3(0.5f*maze._width / 0.1f)));
		_markerShader->setVec4Uniform("color", glm::vec4(1.0f, 1.0f, 1.0f, 1.0f));

		_marker->draw();
	}

	glStencilFunc(GL_NOTEQUAL, 0.0f, 1.0f);
	glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);

	{
		_markerShader->use();
		_markerShader->setMat4Uniform("mvpMatrix", camera.projMatrix * camera.viewMatrix *
			glm::translate(glm::mat4(1.0f), _head_light.position));
		_markerShader->setMat4Uniform("scaleMatrix", glm::scale(glm::vec3(0.5f*maze._width / 0.1f)));
		_markerShader->setVec4Uniform("color", glm::vec4(0.0f, 0.0f, 0.0f, 1.0f));

		_marker->draw();
	}

	glClearStencil(0);
	glClear(GL_STENCIL_BUFFER_BIT);
	glDisable(GL_STENCIL_TEST);
}

void MazeApplication::drawScene(CameraInfo& camera, bool is_portal_placed) {
	int width, height;

	glfwGetFramebufferSize(_window, &width, &height);
	glViewport(0, 0, width, height);

	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);

	//Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	//------------------------------------------------------
	//------------------------------------------------------

	glm::vec3 lightPosCamSpace;
	lightPosCamSpace = glm::vec3(camera.viewMatrix * glm::vec4(_light.position, 1.0));

	//------------------------------------------------------
	//------------------------------------------------------

	_shader->use();

	_shader->setMat4Uniform("viewMatrix", camera.viewMatrix);
	_shader->setMat4Uniform("projectionMatrix", camera.projMatrix);

	_shader->setVec3Uniform("light.pos", lightPosCamSpace);
	_shader->setVec3Uniform("light.La", _light.ambient);
	_shader->setVec3Uniform("light.Ld", _light.diffuse);
	_shader->setVec3Uniform("light.Ls", _light.specular);
	_shader->setVec3Uniform("light.a", glm::vec3(_light.attenuation0, 
												 _light.attenuation1, 
												 _light.attenuation2));

	//------------------------------------------------------
	//------------------------------------------------------

	_shader->setVec3Uniform("material.Ka", _wall_material.Ka);
	_shader->setVec3Uniform("material.Kd", _wall_material.Kd);
	_shader->setVec3Uniform("material.Ks", _wall_material.Ks);
	_shader->setFloatUniform("material.shininess", _wall_material.shininess);
	_shader->setIntUniform("material.haveNormalMap", _wall_material.haveNormalMap);

	drawMesh(_shader, camera, _maze._maze, _wallTexture, _wallNormalMap);

	if (_maze._isCeilVIsible) {
		drawMesh(_shader, camera, _maze._ceiling, _wallTexture, _wallNormalMap);
	}

	//------------------------------------------------------
	//------------------------------------------------------
	
	_shader->setVec3Uniform("material.Ka", _floor_material.Ka);
	_shader->setVec3Uniform("material.Kd", _floor_material.Kd);
	_shader->setVec3Uniform("material.Ks", _floor_material.Ks);
	_shader->setFloatUniform("material.shininess", _floor_material.shininess);
	_shader->setIntUniform("material.haveNormalMap", _floor_material.haveNormalMap);

	drawMesh(_shader, camera, _maze._floor, _floorTexture, _floorNormalMap);

	//------------------------------------------------------
	//------------------------------------------------------

	_shader->setVec3Uniform("material.Ka", _poster_material.Ka);
	_shader->setVec3Uniform("material.Kd", _poster_material.Kd);
	_shader->setVec3Uniform("material.Ks", _poster_material.Ks);
	_shader->setFloatUniform("material.shininess", _poster_material.shininess);
	_shader->setIntUniform("material.haveNormalMap", _poster_material.haveNormalMap);

	for (int i = 0; i < _maze._numPosters; i++) {
		drawMesh(_shader, camera, _maze._posters[i], _posterTextures[i], _posterTextures[i]);
	}

	//------------------------------------------------------
	//------------------------------------------------------

	{
		_markerShader->use();
		_markerShader->setMat4Uniform("mvpMatrix", camera.projMatrix * camera.viewMatrix * glm::translate(glm::mat4(1.0f), _light.position));
		_markerShader->setMat4Uniform("scaleMatrix", glm::mat4(1.0f));
		_markerShader->setVec4Uniform("color", glm::vec4(1.0f, 1.0f, 1.0f, 1.0f));
		_marker->draw();
	}

	drawPortals(_map_shader, camera, _portalTextures, is_portal_placed);

	//------------------------------------------------------
	//------------------------------------------------------

	glDisable(GL_DEPTH_TEST);
	glViewport(width - width / 4, height - width / 4, width / 4, width / 4);

	drawMap(_mapCamera);
}

void MazeApplication::drawToFramebuffer(CameraInfo& camera, GLuint& framebuffer)
{

	glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
	glViewport(0, 0, _fbWidth, _fbHeight);

	//glDisable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);

	//Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	//------------------------------------------------------
	//------------------------------------------------------

	glm::vec3 lightPosCamSpace;
	lightPosCamSpace = glm::vec3(camera.viewMatrix * glm::vec4(_light.position, 1.0));

	//------------------------------------------------------
	//------------------------------------------------------

	_shader->use();

	_shader->setMat4Uniform("viewMatrix", camera.viewMatrix);
	_shader->setMat4Uniform("projectionMatrix", camera.projMatrix);

	_shader->setVec3Uniform("light.pos", lightPosCamSpace);
	_shader->setVec3Uniform("light.La", _light.ambient);
	_shader->setVec3Uniform("light.Ld", _light.diffuse);
	_shader->setVec3Uniform("light.Ls", _light.specular);
	_shader->setVec3Uniform("light.a", glm::vec3(_light.attenuation0,
												_light.attenuation1,
												_light.attenuation2));

	//------------------------------------------------------
	//------------------------------------------------------

	_shader->setVec3Uniform("material.Ka", _wall_material.Ka);
	_shader->setVec3Uniform("material.Kd", _wall_material.Kd);
	_shader->setVec3Uniform("material.Ks", _wall_material.Ks);
	_shader->setFloatUniform("material.shininess", _wall_material.shininess);
	_shader->setIntUniform("material.haveNormalMap", _wall_material.haveNormalMap);

	drawMesh(_shader, camera, _maze._maze, _wallTexture, _wallNormalMap);

	if (_maze._isCeilVIsible) {
		drawMesh(_shader, camera, _maze._ceiling, _wallTexture, _wallNormalMap);
	}

	//------------------------------------------------------
	//------------------------------------------------------

	_shader->setVec3Uniform("material.Ka", _floor_material.Ka);
	_shader->setVec3Uniform("material.Kd", _floor_material.Kd);
	_shader->setVec3Uniform("material.Ks", _floor_material.Ks);
	_shader->setFloatUniform("material.shininess", _floor_material.shininess);
	_shader->setIntUniform("material.haveNormalMap", _floor_material.haveNormalMap);

	drawMesh(_shader, camera, _maze._floor, _floorTexture, _floorNormalMap);

	//------------------------------------------------------
	//------------------------------------------------------

	_shader->setVec3Uniform("material.Ka", _poster_material.Ka);
	_shader->setVec3Uniform("material.Kd", _poster_material.Kd);
	_shader->setVec3Uniform("material.Ks", _poster_material.Ks);
	_shader->setFloatUniform("material.shininess", _poster_material.shininess);
	_shader->setIntUniform("material.haveNormalMap", _poster_material.haveNormalMap);

	for (int i = 0; i < _maze._numPosters; i++) {
		drawMesh(_shader, camera, _maze._posters[i], _posterTextures[i], _posterTextures[i]);
	}

	//------------------------------------------------------
	//------------------------------------------------------

	{
		_markerShader->use();
		_markerShader->setMat4Uniform("mvpMatrix", camera.projMatrix * camera.viewMatrix * glm::translate(glm::mat4(1.0f), _light.position));
		_markerShader->setMat4Uniform("scaleMatrix", glm::mat4(1.0f));
		_markerShader->setVec4Uniform("color", glm::vec4(1.0f, 1.0f, 1.0f, 1.0f));
		_marker->draw();
	}

	drawPortals(_map_shader, camera, _portalTextures, false);

	//------------------------------------------------------
	//------------------------------------------------------

	glBindSampler(0, 0);
	glUseProgram(0);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}




int main()
{
	MazeApplication app;
	app.start();

	return 0;
}