#pragma once

#include <vector>
#include<iostream>
#include <cmath>

class MazeLayout {
public:
	MazeLayout() {}
	~MazeLayout() {}
	MazeLayout(int size, float width, std::vector <std::vector<bool>> maze);

	std::pair<float, float> resolveColision(float x, float y, float old_x, float old_y);

	int _size;
	std::vector<std::vector<bool>> _maze;
	float _width;
	float _height;
	float _pad = 0.1;
	float _center;
};

MazeLayout::MazeLayout(int size, float width, std::vector <std::vector<bool>> maze) {
	_size = size;
	_width = width;
	_height = 2 * width;
	_maze = std::vector<std::vector<bool>>(_size + 2, std::vector<bool>(_size + 2, false));
	for (int i = 1; i < _size + 1; i++) {
		for (int j = 1; j < _size + 1; j++) {
			_maze[i][j] = maze[i - 1][j - 1];
		}
	}
	_center = 0.5f*_width*_size;
}

std::pair<float, float> MazeLayout::resolveColision(float x, float y, float old_x, float old_y) {
	int maze_ox = std::floor(old_x / _width) + 1;
	int maze_oy = std::floor(old_y / _width) + 1;

	float new_x = x;
	float new_y = y;

	if (!((-_pad * _width < x) && (x < _size*_width + _pad * _width)
		&& (-_pad * _width < y) && (y < _size*_width + _pad * _width))) {
		return std::make_pair(new_x, new_y);
	}

	int pos_pad_maze_x = std::floor(x / _width + _pad) + 1;
	int neg_pad_maze_x = std::floor(x / _width - _pad) + 1;
	int pos_pad_maze_y = std::floor(y / _width + _pad) + 1;
	int neg_pad_maze_y = std::floor(y / _width - _pad) + 1;
	int zero_pad_maze_x = std::floor(x / _width) + 1;
	int zero_pad_maze_y = std::floor(y / _width) + 1;
	
	if (!((_maze[pos_pad_maze_y][pos_pad_maze_x]) || 
		  (_maze[pos_pad_maze_y][neg_pad_maze_x]) ||
		  (_maze[neg_pad_maze_y][pos_pad_maze_x]) || 
		  (_maze[neg_pad_maze_y][neg_pad_maze_x]))) {
		return std::make_pair(new_x, new_y);
	}
	
	//std::cout << _maze[pos_pad_maze_y][neg_pad_maze_x] << _maze[pos_pad_maze_y][zero_pad_maze_x] << _maze[pos_pad_maze_y][pos_pad_maze_x] << "\n";
	//std::cout << _maze[zero_pad_maze_y][neg_pad_maze_x] << _maze[zero_pad_maze_y][zero_pad_maze_x] << _maze[zero_pad_maze_y][pos_pad_maze_x] << "\n";
	//std::cout << _maze[neg_pad_maze_y][neg_pad_maze_x] << _maze[neg_pad_maze_y][zero_pad_maze_x] << _maze[neg_pad_maze_y][pos_pad_maze_x] << "\n";
	
	if (_maze[pos_pad_maze_y][pos_pad_maze_x] +
		_maze[pos_pad_maze_y][neg_pad_maze_x] +
		_maze[pos_pad_maze_y][zero_pad_maze_x] +
		_maze[zero_pad_maze_y][pos_pad_maze_x] +
		_maze[zero_pad_maze_y][neg_pad_maze_x] +
		_maze[zero_pad_maze_y][zero_pad_maze_x] +
		_maze[neg_pad_maze_y][pos_pad_maze_x] +
		_maze[neg_pad_maze_y][neg_pad_maze_x] +
		_maze[neg_pad_maze_y][zero_pad_maze_x] == 9) {

		return std::make_pair(old_x, old_y);
	}

	bool easy_case = false;
	if (_maze[pos_pad_maze_y][pos_pad_maze_x] + 
		_maze[pos_pad_maze_y][neg_pad_maze_x] +
		_maze[pos_pad_maze_y][zero_pad_maze_x] == 3) {

		new_y = (pos_pad_maze_y - 1 - _pad)*_width;
		easy_case = true;
	}
	if (_maze[neg_pad_maze_y][pos_pad_maze_x] +
		_maze[neg_pad_maze_y][neg_pad_maze_x] +
		_maze[neg_pad_maze_y][zero_pad_maze_x] == 3) {

		new_y = (neg_pad_maze_y + _pad)*_width;
		easy_case = true;
	}
	
	if (_maze[pos_pad_maze_y][pos_pad_maze_x] +
		_maze[neg_pad_maze_y][pos_pad_maze_x] +
		_maze[zero_pad_maze_y][pos_pad_maze_x] == 3) {

		new_x = (pos_pad_maze_x - 1 - _pad)*_width;
		easy_case = true;
	}
	if (_maze[pos_pad_maze_y][neg_pad_maze_x] +
		_maze[neg_pad_maze_y][neg_pad_maze_x] +
		_maze[zero_pad_maze_y][neg_pad_maze_x] == 3) {

		new_x = (neg_pad_maze_x + _pad)*_width;
		easy_case = true;
	}

	if (!easy_case) {
		if (_maze[pos_pad_maze_y][pos_pad_maze_x] +
			_maze[pos_pad_maze_y][neg_pad_maze_x] +
			_maze[pos_pad_maze_y][zero_pad_maze_x] > 0) {
			if (_maze[pos_pad_maze_y][pos_pad_maze_x]) {

				new_x = (pos_pad_maze_x - 1 - _pad)*_width;
				new_y = (pos_pad_maze_y - 1 - _pad)*_width;
			}
			else {
				new_x = (neg_pad_maze_x + _pad)*_width;
				new_y = (pos_pad_maze_y - 1 - _pad)*_width;
			}
			if (std::abs(new_x - x) >= std::abs(new_y - y)) {
				new_x = x;
			}
			else {
				new_y = y;
			}
		}

		if (_maze[neg_pad_maze_y][pos_pad_maze_x] +
			_maze[neg_pad_maze_y][neg_pad_maze_x] +
			_maze[neg_pad_maze_y][zero_pad_maze_x] > 0) {
			if (_maze[neg_pad_maze_y][neg_pad_maze_x]) {

				new_x = (neg_pad_maze_x + _pad)*_width;
				new_y = (neg_pad_maze_y + _pad)*_width;
			}
			else {
				new_x = (pos_pad_maze_x - 1 - _pad)*_width;
				new_y = (neg_pad_maze_y + _pad)*_width;
			}
			if (std::abs(new_x - x) >= std::abs(new_y - y)) {
				new_x = x;
			}
			else {
				new_y = y;
			}
		}
	}

	int maze_x = std::floor(new_x / _width) + 1;
	int maze_y = std::floor(new_y / _width) + 1;

	return std::make_pair(new_x, new_y);
}