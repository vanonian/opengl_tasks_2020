#pragma once 

#include <Mesh.hpp>

#include <iostream>
#include <vector>
#include <fstream>
#include <string>

#include "MazeLayout.hpp"
#include "MazeFreeCameraMover.hpp"

class MazeMeshes {
public:
	MazeMeshes() {}
	~MazeMeshes() {}
	void makeMaze(std::vector<std::string>& paths, glm::vec3& start, MazeLayout& maze);
	void portalUpdate(int portal, CameraMoverPtr& camera, MazeLayout& maze, bool isHeadLight, CameraMoverPtr& freeCamera);
	void findPortalPos(glm::vec3& pos, glm::vec3& dir, const int portal, MazeLayout& maze);
	void findPortalPos(int portal, CameraMoverPtr& camera, MazeLayout& maze, bool isHeadLight); 

	MeshPtr _maze;
	MeshPtr _floor;
	MeshPtr _ceiling;
	std::vector<std::vector<MeshPtr>> _portals;
	std::vector<MeshPtr> _posters;
	std::vector<MeshPtr>  _map_portals;

	int _numPosters;

	bool _isCeilVIsible = false;

	std::vector<bool> _is_portal_placed = std::vector<bool>(2, false);
	std::vector<bool> _is_portal_processed = std::vector<bool>(2, false);
	std::vector<int> _portal_positioned = std::vector<int>(2, -1);
	std::vector <std::pair<int, int>> _portal_position = std::vector <std::pair<int, int>>(2, std::make_pair(-1,-1));

};

void MazeMeshes::makeMaze(std::vector<std::string>& paths,  glm::vec3& start, MazeLayout& maze) {

	std::ifstream f;
	f.open(paths[0]);

	int size;
	float width;

	f >> size;
	f >> width;
	f >> start.x >> start.y >> start.z;

	std::vector<std::vector<bool>> maze_map(size, std::vector<bool>(size));
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {
			bool cell;
			f >> cell;
			maze_map[i][j] = cell;
		}
	}
	maze = MazeLayout(size, width, maze_map);
	_numPosters = 6;
	f.close();

	char c;
	float x, y, z;

	_maze = std::make_shared<Mesh>();
	_ceiling = std::make_shared<Mesh>();
	_floor = std::make_shared<Mesh>();
	
	_posters = std::vector<MeshPtr>(_numPosters);
	for (int i = 0; i < _numPosters; i++) {
		_posters[i] = std::make_shared<Mesh>();
	}
	_portals = std::vector<std::vector<MeshPtr>>(2);
	for (int i = 0; i < 2; i++) {
		_portals[i] = std::vector<MeshPtr>(4);
		for (int j = 0; j < 4; j++) {
			_portals[i][j] = std::make_shared<Mesh>();
		}
	}

	_map_portals = std::vector<MeshPtr>(2);
	for (int i = 0; i < 2; i++) {
		_map_portals[i] = std::make_shared<Mesh>();
	}


	std::vector<glm::vec3> vertices = {glm::vec3(-0.5*width, -0.25*width, 2.0*width), glm::vec3(0.5*width,  -0.25*width, 2.0*width),
									   glm::vec3(-0.5*width,  0.25*width, 2.0*width), glm::vec3(0.5*width,  0.25*width, 2.0*width)};
	std::vector<glm::vec3> normals = {glm::vec3(0.0, 0.0, 1.0), glm::vec3(0.0, 0.0, 1.0),
									  glm::vec3(0.0, 0.0, 1.0),glm::vec3(0.0, 0.0, 1.0)};
	std::vector<glm::vec3> tangents = { glm::vec3(0.0, 0.0, 1.0), glm::vec3(0.0, 0.0, 1.0),
									  glm::vec3(0.0, 0.0, 1.0),glm::vec3(0.0, 0.0, 1.0) };
	std::vector<unsigned int> indices = {0,1,2,2,1,3};
	std::vector<glm::vec2> texcoords = { glm::vec2(0.0,0.0), glm::vec2(1.0,0.0),
										glm::vec2(0.0,1.0), glm::vec2(1.0,1.0) };
	for (int i = 0; i < 2; i++) {
		DataBufferPtr vert = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
		DataBufferPtr norm = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
		DataBufferPtr tang = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
		DataBufferPtr tex = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
		DataBufferPtr idx = std::make_shared<DataBuffer>(GL_ELEMENT_ARRAY_BUFFER);

		vert->setData(vertices.size() * sizeof(float) * 3, vertices.data());
		norm->setData(normals.size() * sizeof(float) * 3, normals.data());
		tang->setData(tangents.size() * sizeof(float) * 3, tangents.data());
		tex->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());
		idx->setData(indices.size() * sizeof(unsigned int), indices.data());

		_map_portals[i]->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, vert);
		_map_portals[i]->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, norm);
		_map_portals[i]->setAttribute(2, 3, GL_FLOAT, GL_FALSE, 0, 0, tang);
		_map_portals[i]->setAttribute(3, 2, GL_FLOAT, GL_FALSE, 0, 0, tex);
		_map_portals[i]->setIndices(indices.size(), idx);
		_map_portals[i]->setPrimitiveType(GL_TRIANGLES);
		_map_portals[i]->setVertexCount(vertices.size());
	}

	for (int i = 1; i < 6; i++) {
		std::vector<glm::vec3> vertices;
		std::vector<glm::vec3> normals;
		std::vector<glm::vec3> tangents;
		std::vector<unsigned int> indices;
		std::vector<glm::vec2> texcoords;

		f.open(paths[i]);
		while (f >> c >> x >> y >> z)
		{
			if (c == 'v')
				vertices.push_back(glm::vec3(x*width, y*width, z*width));
			if (c == 'n')
				normals.push_back(glm::vec3(x, y, z));
			if (c == 't')
				tangents.push_back(glm::vec3(x, y, z));
			if (c == 'x')
				texcoords.push_back(glm::vec2(x, y));
			if (c == 'f') {
				indices.push_back(unsigned int(x));
				indices.push_back(unsigned int(y));
				indices.push_back(unsigned int(z));
			}

		}

		f.close();
		//----------------------------------------

		DataBufferPtr vert = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
		DataBufferPtr norm = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
		DataBufferPtr tang = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
		DataBufferPtr tex = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
		DataBufferPtr idx = std::make_shared<DataBuffer>(GL_ELEMENT_ARRAY_BUFFER);

		vert->setData(vertices.size() * sizeof(float) * 3, vertices.data());
		norm->setData(normals.size() * sizeof(float) * 3, normals.data());
		tang->setData(tangents.size() * sizeof(float) * 3, tangents.data());
		tex->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());
		idx->setData(indices.size() * sizeof(unsigned int), indices.data());

		if (i == 1) {
			_maze->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, vert);
			_maze->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, norm);
			_maze->setAttribute(2, 3, GL_FLOAT, GL_FALSE, 0, 0, tang);
			_maze->setAttribute(3, 2, GL_FLOAT, GL_FALSE, 0, 0, tex);
			_maze->setIndices(indices.size(), idx);
			_maze->setPrimitiveType(GL_TRIANGLES);
			_maze->setVertexCount(vertices.size());
			std::cout << "Maze is created with " << _maze->getVertexCount() << " vertices\n";
		}
		if (i == 2) {
			_ceiling->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, vert);
			_ceiling->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, norm);
			_ceiling->setAttribute(2, 3, GL_FLOAT, GL_FALSE, 0, 0, tang);
			_ceiling->setAttribute(3, 2, GL_FLOAT, GL_FALSE, 0, 0, tex);
			_ceiling->setIndices(indices.size(), idx);
			_ceiling->setPrimitiveType(GL_TRIANGLES);
			_ceiling->setVertexCount(vertices.size());
			std::cout << "Ceiling is created with " << _ceiling->getVertexCount() << " vertices\n";
		}
		if (i == 3) {
			_floor->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, vert);
			_floor->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, norm);
			_floor->setAttribute(2, 3, GL_FLOAT, GL_FALSE, 0, 0, tang);
			_floor->setAttribute(3, 2, GL_FLOAT, GL_FALSE, 0, 0, tex);
			_floor->setIndices(indices.size(), idx);
			_floor->setPrimitiveType(GL_TRIANGLES);
			_floor->setVertexCount(vertices.size());
			std::cout << "Floor is created with " << _floor->getVertexCount() << " vertices\n";
		}

		if (i == 4) {
			int vert_size = vertices.size() / _numPosters;
			int norm_size = vert_size;
			int tang_size = norm_size;
			int tex_size = tang_size;
			int idx_size = indices.size() / _numPosters;

			for (int j = 0; j < _numPosters; j++) {
				vert = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
				norm = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
				tang = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
				tex = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
				idx = std::make_shared<DataBuffer>(GL_ELEMENT_ARRAY_BUFFER);

				std::vector<glm::vec3> vr(vert_size);
				std::vector<glm::vec3> nr(norm_size);
				std::vector<glm::vec3> tn(tang_size);
				std::vector<unsigned int> in(idx_size);
				std::vector<glm::vec2> tx(tex_size);
				for (int k = 0; k < vert_size; k++) {
					vr[k] = vertices[k + j * vert_size];
					nr[k] = normals[k + j * norm_size];
					tn[k] = tangents[k + j * tang_size];
					tx[k] = texcoords[k + j * tex_size];
				}
				for (int k = 0; k < idx_size; k++) {
					in[k] = indices[k + j * idx_size];
				}

				vert->setData(vert_size * sizeof(float) * 3, vr.data());
				norm->setData(norm_size * sizeof(float) * 3, nr.data());
				tang->setData(tang_size * sizeof(float) * 3, tn.data());
				tex->setData(tex_size * sizeof(float) * 2, tx.data());
				idx->setData(idx_size * sizeof(unsigned int), in.data());

				_posters[j]->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, vert);
				_posters[j]->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, norm);
				_posters[j]->setAttribute(2, 3, GL_FLOAT, GL_FALSE, 0, 0, tang);
				_posters[j]->setAttribute(3, 2, GL_FLOAT, GL_FALSE, 0, 0, tex);
				_posters[j]->setIndices(idx_size, idx);
				_posters[j]->setPrimitiveType(GL_TRIANGLES);
				_posters[j]->setVertexCount(vert_size);
				std::cout << "Poster " << j << " is created with " << _posters[j]->getVertexCount() << " vertices\n";
			}
		}

		if (i == 5) {
			int vert_size = vertices.size() / 4;
			int norm_size = vert_size;
			int tang_size = norm_size;
			int tex_size = tang_size;
			int idx_size = indices.size() / 4;

			for (int j = 0; j < 4; j++) {
				vert = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
				norm = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
				tang = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
				tex = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
				idx = std::make_shared<DataBuffer>(GL_ELEMENT_ARRAY_BUFFER);

				std::vector<glm::vec3> vr(vert_size);
				std::vector<glm::vec3> nr(norm_size);
				std::vector<glm::vec3> tn(tang_size);
				std::vector<unsigned int> in(idx_size);
				std::vector<glm::vec2> tx(tex_size);
				for (int k = 0; k < vert_size; k++) {
					vr[k] = vertices[k + j * vert_size];
					nr[k] = normals[k + j * norm_size];
					tn[k] = tangents[k + j * tang_size];
					tx[k] = texcoords[k + j * tex_size];
				}
				for (int k = 0; k < idx_size; k++) {
					in[k] = indices[k + j * idx_size];
				}

				vert->setData(vert_size * sizeof(float) * 3, vr.data());
				norm->setData(norm_size * sizeof(float) * 3, nr.data());
				tang->setData(tang_size * sizeof(float) * 3, tn.data());
				tex->setData(tex_size * sizeof(float) * 2, tx.data());
				idx->setData(idx_size * sizeof(unsigned int), in.data());

				for (int k = 0; k < 2; k++) {
					_portals[k][j]->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, vert);
					_portals[k][j]->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, norm);
					_portals[k][j]->setAttribute(2, 3, GL_FLOAT, GL_FALSE, 0, 0, tang);
					_portals[k][j]->setAttribute(3, 2, GL_FLOAT, GL_FALSE, 0, 0, tex);
					_portals[k][j]->setIndices(idx_size, idx);
					_portals[k][j]->setPrimitiveType(GL_TRIANGLES);
					_portals[k][j]->setVertexCount(vert_size);
					std::cout << "Portal " << j + k*4 << " is created with " << _portals[k][j]->getVertexCount() << " vertices\n";
				}
			}
		}
	}
}

void MazeMeshes::portalUpdate(int portal, CameraMoverPtr& camera, MazeLayout& maze, bool isHeadLight, CameraMoverPtr& freecamera) {

	if (_is_portal_processed[1 - portal]) {

		_is_portal_processed[1 - portal] = false;
		_portal_position[1 - portal] = std::make_pair(-1, -1);
	}


	if (_is_portal_placed[portal]) {

		_is_portal_placed[portal] = false;
		std::static_pointer_cast<MazeFreeCameraMover>(freecamera)->_portal_placed[portal] = false;
		_is_portal_processed[portal] = true;
	}
	else if (!_is_portal_processed[portal]) {

		_is_portal_processed[portal] = true;
	}
	else {
		findPortalPos(portal, camera, maze, isHeadLight);
		if (_portal_positioned[portal] != -1) {

			_is_portal_placed[portal] = true;
			std::static_pointer_cast<MazeFreeCameraMover>(freecamera)->_portal_placed[portal] = true;
			std::static_pointer_cast<MazeFreeCameraMover>(freecamera)->_portal_pos[portal] = _portal_position[portal];
			std::static_pointer_cast<MazeFreeCameraMover>(freecamera)->_portal_faces[portal] = _portal_positioned[portal];
			_is_portal_processed[portal] = false;
		}
	}
}

void MazeMeshes::findPortalPos(int portal, CameraMoverPtr& camera, MazeLayout& maze, bool isHeadLight) {
	if (isHeadLight) {

		glm::vec3 position = std::static_pointer_cast<MazeFreeCameraMover>(camera)->getPosition();
		glm::mat4 viewMatrix = camera->cameraInfo().viewMatrix;
		glm::vec3 direction = glm::vec3(-viewMatrix[0][2], -viewMatrix[1][2], -viewMatrix[2][2]);

		findPortalPos(position, direction, portal, maze);
	}
}

void MazeMeshes::findPortalPos(glm::vec3& pos, glm::vec3& dir, int portal, MazeLayout& maze) {
	_portal_positioned[portal] = -1;
	int maze_x = std::floor(pos.x / maze._width) + 1;
	int maze_y = std::floor(pos.y / maze._width) + 1;

	int new_maze_x, new_maze_y;
	std::vector<std::pair<int, int>> posible_pos;
	std::vector<std::pair<float, float>> real_posible_pos;
	std::vector<int> posible_faces;

	if (dir.x > 0) {
		if (maze_x < 0) {
			new_maze_x = 0;
		}
		else {
			new_maze_x = 2*((maze_x+1)/2);
		}

		for (; new_maze_x < maze._size + 1; new_maze_x += 2) {
			float new_real_z = pos.z + (new_maze_x*maze._width - pos.x)*(dir.z / dir.x);

			if (new_real_z<0 || new_real_z>maze._height) {
				break;
			}

			float new_real_y = pos.y + (new_maze_x*maze._width - pos.x)*(dir.y / dir.x);
			new_maze_y = std::floor(new_real_y / maze._width) + 1;

			if (new_maze_y < maze._size + 1 && new_maze_y > 0) {
				if (maze._maze[new_maze_y][new_maze_x+1]) {
					if (!maze._maze[new_maze_y][new_maze_x]) {

						posible_pos.push_back(std::make_pair(new_maze_y, new_maze_x + 1));
						real_posible_pos.push_back(std::make_pair(new_real_y, new_maze_x*maze._width));
						posible_faces.push_back(2);
					}
					break;
				}
			}
			else {
				break;
			}
		}
	}

	if (dir.x < 0) {
		if (maze_x > maze._size+1) {

			new_maze_x = maze._size;
		}
		else {
			new_maze_x = 2 * (maze_x / 2)-1;
		}

		for (; new_maze_x > 0; new_maze_x -= 2) {
			
			float new_real_z = pos.z + (new_maze_x*maze._width - pos.x)*(dir.z / dir.x);
			if (new_real_z<0 || new_real_z>maze._height) {
				break;
			}

			float new_real_y = pos.y + (new_maze_x*maze._width - pos.x)*(dir.y / dir.x);
			new_maze_y = std::floor(new_real_y / maze._width) + 1;

			if (new_maze_y < maze._size + 1 && new_maze_y > 0) {
				if (maze._maze[new_maze_y][new_maze_x]) {
					if (!maze._maze[new_maze_y][new_maze_x + 1]) {

						posible_pos.push_back(std::make_pair(new_maze_y, new_maze_x));
						real_posible_pos.push_back(std::make_pair(new_real_y, new_maze_x*maze._width));
						posible_faces.push_back(3);
					}
					break;
				}
			}
			else {
				break;
			}
		}
	}
	
	if (dir.y > 0) {
		if (maze_y < 0) {

			new_maze_y = 0;
		}
		else {
			new_maze_y = 2 * ((maze_y + 1) / 2);
		}

		for (; new_maze_y < maze._size + 1; new_maze_y += 2) {
			float new_real_z = pos.z + (new_maze_y*maze._width - pos.y)*(dir.z / dir.y);
			if (new_real_z<0 || new_real_z>maze._height) {
				break;
			}

			float new_real_x = pos.x + (new_maze_y*maze._width - pos.y)*(dir.x / dir.y);
			new_maze_x = std::floor(new_real_x / maze._width) + 1;

			if (new_maze_x < maze._size + 1 && new_maze_x > 0) {
				if (maze._maze[new_maze_y+1][new_maze_x]) {
					if (!maze._maze[new_maze_y][new_maze_x]) {

						posible_pos.push_back(std::make_pair(new_maze_y + 1, new_maze_x));
						real_posible_pos.push_back(std::make_pair(new_maze_y*maze._width, new_real_x));
						posible_faces.push_back(0);
					}
					break;
				}
			}
			else {
				break;
			}
		}
	}
	
	if (dir.y < 0) {
		if (maze_y > maze._size + 1) {

			new_maze_y = maze._size;
		}
		else {
			new_maze_y = 2 * (maze_y / 2) - 1;
		}

		for (; new_maze_y > 0; new_maze_y -= 2) {
			float new_real_z = pos.z + (new_maze_y*maze._width - pos.y)*(dir.z / dir.y);
			if (new_real_z<0 || new_real_z>maze._height) {
				break;
			}

			float new_real_x = pos.x + (new_maze_y*maze._width - pos.y)*(dir.x / dir.y);
			new_maze_x = std::floor(new_real_x / maze._width) + 1;

			if (new_maze_x < maze._size + 1 && new_maze_x > 0) {
				if (maze._maze[new_maze_y][new_maze_x]) {
					if (!maze._maze[new_maze_y + 1][new_maze_x]) {

						posible_pos.push_back(std::make_pair(new_maze_y, new_maze_x));
						real_posible_pos.push_back(std::make_pair(new_maze_y*maze._width, new_real_x));
						posible_faces.push_back(1);
					}
					break;
				}
			}
			else {
					break;
			}
		}
	}

	
	if (posible_faces.size()>0) {

		int face = posible_faces[0];
		int index = 0;

		for (int i = 1; i < posible_faces.size(); i++) {
			if (std::abs(real_posible_pos[i].first-pos.y) + std::abs(real_posible_pos[i].second-pos.x)
				< std::abs(real_posible_pos[index].first-pos.y) + std::abs(real_posible_pos[index].second-pos.x)) {

				index = i;
				face = posible_faces[i];
			}
		}

		_portal_position[portal] = std::make_pair(posible_pos[index].second, posible_pos[index].first);
		
		if (_portal_position[0].first == _portal_position[1].first &&
			_portal_position[0].second == _portal_position[1].second &&
			_portal_positioned[1-portal] == face) {
			std::cout << "Well how \n";
			return;
		}

		int y = posible_pos[index].first - 1;
		int x = posible_pos[index].second - 1;
		
		_portals[portal][face]->setModelMatrix(glm::translate(glm::mat4(1.0f), 
											   glm::vec3((x+(1e-2)*(face/2)*(2*(face%2)-1))*maze._width, 
											             (y+(1e-2)*(1-face/2)*(2*(face%2)-1))*maze._width, 
														 0.0f)));

		_map_portals[portal]->setModelMatrix(glm::translate(glm::mat4(1.0f),
			glm::vec3((x+0.5*(1-face/2) + (face/2)*(face%2))*maze._width, (y+0.5*(face/2)+(1-face/2)*(face%2))*maze._width, 0.0f))
			*glm::rotate(glm::mat4(1.0), glm::radians(90.0f*(face / 2)), glm::vec3(0.0, 0.0, 1.0)));

		_portal_positioned[portal] = face;
	}
}
