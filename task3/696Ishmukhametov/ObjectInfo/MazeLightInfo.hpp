#pragma once

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <LightInfo.hpp>

struct MazeLightInfo : LightInfo {

	void setLightParams(glm::vec3 amb,
		glm::vec3 diff, glm::vec3 spec,
		glm::vec3 att)
	{
		ambient = amb;
		diffuse = diff;
		specular = spec;
		attenuation0 = att.x;
		attenuation1 = att.y;
		attenuation2 = att.z;
	}
};