#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

/**
��������� ��������� �������
*/
struct MazeMaterialInfo
{
	///����������� ��������� ����������� �����
	glm::vec3 Ka;

	///����������� ��������� ���������� �����
	glm::vec3 Kd;

	///����������� ��������� ��������� �����
	glm::vec3 Ks;

	///����������� ���� �����
	float shininess;

	///���� ������� ����� ��������
	bool haveNormalMap;

	MazeMaterialInfo() {}

	void setMaterialParams(glm::vec3 ambient,
		glm::vec3 diffuse, glm::vec3 specular,
		float shine, bool have_nm) {
		Ka = ambient;
		Kd = diffuse;
		Ks = specular;
		shininess = shine;
		haveNormalMap = have_nm;
	}
};