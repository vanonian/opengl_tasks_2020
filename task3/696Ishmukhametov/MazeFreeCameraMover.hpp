#pragma once
#include <Camera.hpp>

#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>
#include <imgui.h>

#include <iostream>
#include "MazeLayout.hpp"
#include <iomanip>

class MazeFreeCameraMover : public CameraMover
{
public:
	MazeFreeCameraMover(glm::vec3& start, MazeLayout& maze) {
		_maze = std::make_shared<MazeLayout>(maze);
		_pos = glm::vec3((start.x+0.5)*_maze->_width, (start.y-0.5)*_maze->_width, (start.z + 0.5)*_maze->_height);
		//��� ����� ���-������ ��������� ��������� ���������� ������
		_rot = glm::toQuat(glm::lookAt(_pos, glm::vec3(_pos.x, start.y*_maze->_width, _pos.z), glm::vec3(0.0f, 0.0f, 1.0f)));
		_speed = 1.5*_maze->_width;
	}
	~MazeFreeCameraMover() {}

	void handleKey(GLFWwindow* window, int key, int scancode, int action, int mods) override;
	void handleMouseMove(GLFWwindow* window, double xpos, double ypos) override;
	void handleScroll(GLFWwindow* window, double xoffset, double yoffset) override;
	void update(GLFWwindow* window, double dt) override;
	std::pair<float, float> isTeleported(std::pair<float, float> pos);

	glm::vec3 getPosition() {
		return _pos;
	}

	glm::vec3 _pos;
	glm::quat _rot;

	//��������� ������� ���� �� ���������� �����
	double _oldXPos = 0.0;
	double _oldYPos = 0.0;
	float _speed = 1.0;
	std::shared_ptr<MazeLayout> _maze;
	std::vector <std::pair<int, int>> _portal_pos = std::vector <std::pair<int, int>>(2, std::make_pair(-1, -1));
	std::vector <int> _portal_faces = std::vector<int>(2, -1);
	std::vector<bool> _portal_placed = std::vector<bool>(2, false);
};

void MazeFreeCameraMover::handleKey(GLFWwindow* window, int key, int scancode, int action, int mods)
{
}

void MazeFreeCameraMover::handleMouseMove(GLFWwindow* window, double xpos, double ypos)
{
	int state = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);
	if (state == GLFW_PRESS)
	{
		double dx = xpos - _oldXPos;
		double dy = ypos - _oldYPos;

		//��������� ��������� ������� �����/����        
		glm::vec3 rightDir = glm::vec3(1.0f, 0.0f, 0.0f) * _rot;
		_rot *= glm::angleAxis(static_cast<float>(dy * 0.005), rightDir);

		//��������� ��������� ������� ������ ������������ ���
		glm::vec3 upDir(0.0f, 0.0f, 1.0f);
		_rot *= glm::angleAxis(static_cast<float>(dx * 0.005), upDir);
	}

	_oldXPos = xpos;
	_oldYPos = ypos;
}

void MazeFreeCameraMover::handleScroll(GLFWwindow* window, double xoffset, double yoffset)
{
}

void MazeFreeCameraMover::update(GLFWwindow* window, double dt)
{
	//�������� ������� ����������� "������" � ������� ������� ���������
	glm::vec3 forwDir = glm::vec3(0.0f, 0.0f, -1.0f) * _rot;
	forwDir.z = 0.0f;
	//�������� ������� ����������� "������" � ������� ������� ���������
	glm::vec3 rightDir = glm::vec3(1.0f, 0.0f, 0.0f) * _rot;

	glm::vec3 oldPos = _pos;
	//������� ������ ������/�����
	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
	{
		_pos += forwDir * _speed * static_cast<float>(dt);
	}
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
	{
		_pos -= forwDir * _speed * static_cast<float>(dt);
	}
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
	{
		_pos -= rightDir * _speed * static_cast<float>(dt);
	}
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
	{
		_pos += rightDir * _speed * static_cast<float>(dt);
	}
	//-----------------------------------------
	std::setprecision(10);
	std::pair<float, float> corr_pos;
	corr_pos = _maze->resolveColision(_pos.x, _pos.y, oldPos.x, oldPos.y);
	corr_pos = isTeleported(corr_pos);

	_pos.x = corr_pos.first;
	_pos.y = corr_pos.second;

	//��������� ����������� � ������� ������
	_camera.viewMatrix = glm::toMat4(-_rot) * glm::translate(-_pos);

	//-----------------------------------------

	int width, height;
	glfwGetFramebufferSize(window, &width, &height);

	//��������� ������� �������� �� ������, ���� ������� ���� ����������
	_camera.projMatrix = glm::perspective(glm::radians(45.0f), (float)width / height, _near, _far);
}

std::pair<float, float> MazeFreeCameraMover::isTeleported(std::pair<float, float> pos) {
	if (!(_portal_placed[0] && _portal_placed[1])) {
		return std::make_pair(pos.first, pos.second);
	}

	std::pair<float, float> cur_pos = std::make_pair(pos.first, pos.second);

	for (int i = 0; i < 2; i++) {
		std::pair<int, int> portal_position = _portal_pos[i];

		int face = _portal_faces[i];

		float pad = _maze->_pad;
		float w = _maze->_width;

		int maze_x = std::floor((pos.first + 2*pad*w*(face/2)*(1-2*(face%2))) / w) + 1;
		int maze_y = std::floor((pos.second + 2 * pad*w*(1-face / 2)*(1 - 2 * (face % 2))) / w) + 1;

		if (maze_x == portal_position.first && maze_y == portal_position.second) {

			if (face / 2 == 1) {
				
				if ((pos.second / w) > portal_position.second-1 + 0.2 && (pos.second / w) < portal_position.second-1 + 0.8) {
					
					int s_face = _portal_faces[1-i];
					cur_pos.first = (_portal_pos[1 - i].first-0.5 + (0.5 + 5 * pad)*(s_face / 2)*(2 * (s_face % 2) - 1))*w;
					cur_pos.second = (_portal_pos[1 - i].second-0.5 + (0.5 + 5 * pad)*(1-s_face / 2)*(2 * (s_face % 2) - 1))*w;
					
					if (face / 2 == s_face / 2) {

						_rot = glm::toQuat(glm::toMat4(_rot)*glm::rotate(glm::mat4(1.0), glm::radians(180.0f - 180.0f*(std::abs(s_face - face))), glm::vec3(0.0f, 0.0f, 1.0f)));
					}
					else {
						if (face < s_face) {
							_rot = glm::toQuat(glm::toMat4(_rot)*glm::rotate(glm::mat4(1.0), glm::radians(-90.0f*(1 - 2 * ((s_face - face) % 2))), glm::vec3(0.0f, 0.0f, 1.0f)));
						}
						else {
							_rot = glm::toQuat(glm::toMat4(_rot)*glm::rotate(glm::mat4(1.0), glm::radians(90.0f*(1 - 2 * ((face - s_face) % 2))), glm::vec3(0.0f, 0.0f, 1.0f)));
						}
					}
				}
				else {
					continue;
				}
			}
			else {
				
				if ((pos.first / w) > portal_position.first - 1 + 0.2 && (pos.first / w) < portal_position.first -1 + 0.8) {
					
					int s_face =_portal_faces[1 - i];
					cur_pos.first = (_portal_pos[1 - i].first - 0.5 + (0.5 + 5 * pad)*(s_face / 2)*(2 * (s_face % 2) - 1))*w;
					cur_pos.second = (_portal_pos[1 - i].second - 0.5 + (0.5 + 5 * pad)*(1 - s_face / 2)*(2 * (s_face % 2) - 1))*w;
 
					if (face / 2 == s_face / 2) {

						_rot = glm::toQuat(glm::toMat4(_rot)*glm::rotate(glm::mat4(1.0), glm::radians(180.0f - 180.0f*(std::abs(s_face - face))), glm::vec3(0.0f, 0.0f, 1.0f)));
					}
					else {
						if (face < s_face) {
							_rot = glm::toQuat(glm::toMat4(_rot)*glm::rotate(glm::mat4(1.0), glm::radians(-90.0f*(1 - 2 * ((s_face - face) % 2))), glm::vec3(0.0f, 0.0f, 1.0f)));
						}
						else {
							_rot = glm::toQuat(glm::toMat4(_rot)*glm::rotate(glm::mat4(1.0), glm::radians(90.0f*(1 - 2 * ((face - s_face) % 2))), glm::vec3(0.0f, 0.0f, 1.0f)));
						}
					}
				}
				else {
					continue;
				}

			}
		}
		else {
			continue;
		}
	}
	return cur_pos;
}
