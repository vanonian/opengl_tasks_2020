#version 330

uniform sampler2D diffuseTex;

in vec2 texCoord; //���������� ���������� (��������������� ����� ��������� ������������)

out vec4 fragColor; //�������� ���� ���������

void main()
{
	vec3 color = texture(diffuseTex, texCoord).rgb;
	fragColor = vec4(color, 0.5);
}
