#version 330

uniform sampler2D diffuseTex;
uniform sampler2D normalMap;

struct LightInfo
{
	vec3 pos; //��������� ��������� ����� � ������� ��������� ����������� ������!
	vec3 dir;
	vec3 La; //���� � ������������� ����������� �����
	vec3 Ld; //���� � ������������� ���������� �����
	vec3 Ls; //���� � ������������� ��������� �����
	vec3 a;
};

struct MaterialInfo
{
	vec3 Ka; //����������� ��������� ����������� �����
	vec3 Kd; //����������� ��������� ���������� �����
	vec3 Ks; //����������� ��������� ���������
	float shininess;
	bool haveNormalMap;
};


uniform LightInfo light;
uniform MaterialInfo material;

in mat3 tbn;
in vec4 posCamSpace; //���������� ������� � ������� ��������� ������ (��������������� ����� ��������� ������������)
in vec2 texCoord; //���������� ���������� (��������������� ����� ��������� ������������)

out vec4 fragColor; //�������� ���� ���������

void main()
{
	vec3 normal;
	vec3 diffuseColor = texture(diffuseTex, texCoord).rgb;
	if (material.haveNormalMap) {
		vec3 normalFromTex = normalize(2*(texture(normalMap, texCoord).rgb-0.5));
		normal = normalize(tbn*normalFromTex);
	} else {
		normal = normalize(tbn*vec3(0,0,1));
	}

	float dd = pow(posCamSpace.x-light.pos.x,2)+pow(posCamSpace.y-light.pos.y,2)+pow(posCamSpace.z-light.pos.z,2);
	float d = sqrt(dd);
	float fading = light.a.x + light.a.y*d + light.a.z*dd;


	vec3 viewDirection = normalize(-posCamSpace.xyz); //����������� �� ����������� ������ (��� ��������� � ����� (0.0, 0.0, 0.0))
	vec3 lightDirCamSpace = normalize(light.pos - posCamSpace.xyz); //����������� �� �������� �����	
	float NdotL = max(dot(normal, lightDirCamSpace.xyz), 0.0); //��������� ������������ (�������)

	vec3 color = diffuseColor * (material.Ka*light.La + material.Kd*light.Ld * NdotL/fading);

	if (NdotL > 0.0)
	{			
		vec3 halfVector = normalize(lightDirCamSpace.xyz + viewDirection); //����������� ����� ������������� �� ������ � �� �������� �����

		float blinnTerm = max(dot(normal, halfVector), 0.0); //������������� ��������� ��������� �� ������				
		blinnTerm = pow(blinnTerm, material.shininess); //���������� ������ �����
		color += material.Ks*light.Ls * blinnTerm/fading;
	}

	fragColor = vec4(color, 1.0);
}
